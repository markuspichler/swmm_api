<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="100000000" symbologyReferenceScale="-1" readOnly="0" hasScaleBasedVisibilityFlag="0" simplifyAlgorithm="0" simplifyLocal="1" styleCategories="AllStyleCategories" simplifyDrawingHints="1" simplifyDrawingTol="1" maxScale="0" simplifyMaxScale="1" version="3.34.4-Prizren" labelsEnabled="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal startField="" endExpression="" mode="0" endField="" durationUnit="min" accumulate="0" fixedDuration="0" limitMode="0" enabled="0" durationField="fid" startExpression="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation symbology="Line" showMarkerSymbolInSurfacePlots="0" extrusion="0" extrusionEnabled="0" type="IndividualFeatures" zscale="1" respectLayerSymbol="1" zoffset="0" clamping="Terrain" binding="Centroid">
    <data-defined-properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0" type="line" name="" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{1dad1180-4db8-4643-89f4-8004556af497}" locked="0" enabled="1" class="SimpleLine">
          <Option type="Map">
            <Option value="0" type="QString" name="align_dash_pattern"/>
            <Option value="square" type="QString" name="capstyle"/>
            <Option value="5;2" type="QString" name="customdash"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="customdash_map_unit_scale"/>
            <Option value="MM" type="QString" name="customdash_unit"/>
            <Option value="0" type="QString" name="dash_pattern_offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="dash_pattern_offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="dash_pattern_offset_unit"/>
            <Option value="0" type="QString" name="draw_inside_polygon"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="231,113,72,255" type="QString" name="line_color"/>
            <Option value="solid" type="QString" name="line_style"/>
            <Option value="0.6" type="QString" name="line_width"/>
            <Option value="MM" type="QString" name="line_width_unit"/>
            <Option value="0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="0" type="QString" name="ring_filter"/>
            <Option value="0" type="QString" name="trim_distance_end"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_end_map_unit_scale"/>
            <Option value="MM" type="QString" name="trim_distance_end_unit"/>
            <Option value="0" type="QString" name="trim_distance_start"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_start_map_unit_scale"/>
            <Option value="MM" type="QString" name="trim_distance_start_unit"/>
            <Option value="0" type="QString" name="tweak_dash_pattern_on_corners"/>
            <Option value="0" type="QString" name="use_custom_dash"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="width_map_unit_scale"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0" type="fill" name="" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{dbec3d34-2f71-4657-8a21-74550de5fb55}" locked="0" enabled="1" class="SimpleFill">
          <Option type="Map">
            <Option value="3x:0,0,0,0,0,0" type="QString" name="border_width_map_unit_scale"/>
            <Option value="231,113,72,255" type="QString" name="color"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="0,0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="165,81,51,255" type="QString" name="outline_color"/>
            <Option value="solid" type="QString" name="outline_style"/>
            <Option value="0.2" type="QString" name="outline_width"/>
            <Option value="MM" type="QString" name="outline_width_unit"/>
            <Option value="solid" type="QString" name="style"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0" type="marker" name="" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{cbdb4435-cdc3-477c-80b8-9052ac2abdcf}" locked="0" enabled="1" class="SimpleMarker">
          <Option type="Map">
            <Option value="0" type="QString" name="angle"/>
            <Option value="square" type="QString" name="cap_style"/>
            <Option value="231,113,72,255" type="QString" name="color"/>
            <Option value="1" type="QString" name="horizontal_anchor_point"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="diamond" type="QString" name="name"/>
            <Option value="0,0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="165,81,51,255" type="QString" name="outline_color"/>
            <Option value="solid" type="QString" name="outline_style"/>
            <Option value="0.2" type="QString" name="outline_width"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="outline_width_map_unit_scale"/>
            <Option value="MM" type="QString" name="outline_width_unit"/>
            <Option value="diameter" type="QString" name="scale_method"/>
            <Option value="3" type="QString" name="size"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="size_map_unit_scale"/>
            <Option value="MM" type="QString" name="size_unit"/>
            <Option value="1" type="QString" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 referencescale="-1" symbollevels="0" type="singleSymbol" forceraster="0" enableorderby="0">
    <symbols>
      <symbol clip_to_extent="1" frame_rate="10" alpha="0.7" force_rhr="0" type="fill" name="0" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{7e559d53-c4ce-4a53-b5ee-dfc64cd4852f}" locked="0" enabled="1" class="GeometryGenerator">
          <Option type="Map">
            <Option value="Line" type="QString" name="SymbolType"/>
            <Option value="make_line(&#xd;&#xa;&#x9;centroid($geometry),&#xd;&#xa;&#x9;coalesce(&#xd;&#xa;&#x9;&#x9;geometry(get_feature('JUNCTIONS', 'name', &quot;SUBCATCHMENTS.outlet&quot;)),&#xd;&#xa;&#x9;&#x9;geometry(get_feature('OUTFALLS', 'name', &quot;SUBCATCHMENTS.outlet&quot;)),&#xd;&#xa;&#x9;&#x9;geometry(get_feature('STORAGE', 'name', &quot;SUBCATCHMENTS.outlet&quot;))&#xd;&#xa;)&#xd;&#xa;)" type="QString" name="geometryModifier"/>
            <Option value="MapUnit" type="QString" name="units"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0" type="line" name="@0@0" is_animated="0">
            <data_defined_properties>
              <Option type="Map">
                <Option value="" type="QString" name="name"/>
                <Option name="properties"/>
                <Option value="collection" type="QString" name="type"/>
              </Option>
            </data_defined_properties>
            <layer pass="0" id="{21dc0571-e7f0-4cf0-ae2a-d61dc0789833}" locked="0" enabled="1" class="SimpleLine">
              <Option type="Map">
                <Option value="0" type="QString" name="align_dash_pattern"/>
                <Option value="square" type="QString" name="capstyle"/>
                <Option value="5;2" type="QString" name="customdash"/>
                <Option value="3x:0,0,0,0,0,0" type="QString" name="customdash_map_unit_scale"/>
                <Option value="MM" type="QString" name="customdash_unit"/>
                <Option value="0" type="QString" name="dash_pattern_offset"/>
                <Option value="3x:0,0,0,0,0,0" type="QString" name="dash_pattern_offset_map_unit_scale"/>
                <Option value="MM" type="QString" name="dash_pattern_offset_unit"/>
                <Option value="0" type="QString" name="draw_inside_polygon"/>
                <Option value="bevel" type="QString" name="joinstyle"/>
                <Option value="255,1,1,255" type="QString" name="line_color"/>
                <Option value="dash" type="QString" name="line_style"/>
                <Option value="0.26" type="QString" name="line_width"/>
                <Option value="MM" type="QString" name="line_width_unit"/>
                <Option value="0" type="QString" name="offset"/>
                <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
                <Option value="MM" type="QString" name="offset_unit"/>
                <Option value="0" type="QString" name="ring_filter"/>
                <Option value="0" type="QString" name="trim_distance_end"/>
                <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_end_map_unit_scale"/>
                <Option value="MM" type="QString" name="trim_distance_end_unit"/>
                <Option value="0" type="QString" name="trim_distance_start"/>
                <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_start_map_unit_scale"/>
                <Option value="MM" type="QString" name="trim_distance_start_unit"/>
                <Option value="0" type="QString" name="tweak_dash_pattern_on_corners"/>
                <Option value="0" type="QString" name="use_custom_dash"/>
                <Option value="3x:0,0,0,0,0,0" type="QString" name="width_map_unit_scale"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" type="QString" name="name"/>
                  <Option name="properties"/>
                  <Option value="collection" type="QString" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
            <layer pass="0" id="{0d9d6356-9232-42e8-8b71-a9fb6d153b3b}" locked="0" enabled="1" class="MarkerLine">
              <Option type="Map">
                <Option value="4" type="QString" name="average_angle_length"/>
                <Option value="3x:0,0,0,0,0,0" type="QString" name="average_angle_map_unit_scale"/>
                <Option value="MM" type="QString" name="average_angle_unit"/>
                <Option value="3" type="QString" name="interval"/>
                <Option value="3x:0,0,0,0,0,0" type="QString" name="interval_map_unit_scale"/>
                <Option value="MM" type="QString" name="interval_unit"/>
                <Option value="0" type="QString" name="offset"/>
                <Option value="0" type="QString" name="offset_along_line"/>
                <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_along_line_map_unit_scale"/>
                <Option value="MM" type="QString" name="offset_along_line_unit"/>
                <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
                <Option value="MM" type="QString" name="offset_unit"/>
                <Option value="true" type="bool" name="place_on_every_part"/>
                <Option value="CentralPoint" type="QString" name="placements"/>
                <Option value="0" type="QString" name="ring_filter"/>
                <Option value="1" type="QString" name="rotate"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" type="QString" name="name"/>
                  <Option name="properties"/>
                  <Option value="collection" type="QString" name="type"/>
                </Option>
              </data_defined_properties>
              <symbol clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0" type="marker" name="@@0@0@1" is_animated="0">
                <data_defined_properties>
                  <Option type="Map">
                    <Option value="" type="QString" name="name"/>
                    <Option name="properties"/>
                    <Option value="collection" type="QString" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="{496d2770-9dc3-4b9c-884f-3f8f3cd88d45}" locked="0" enabled="1" class="SimpleMarker">
                  <Option type="Map">
                    <Option value="90" type="QString" name="angle"/>
                    <Option value="square" type="QString" name="cap_style"/>
                    <Option value="0,0,0,255" type="QString" name="color"/>
                    <Option value="1" type="QString" name="horizontal_anchor_point"/>
                    <Option value="bevel" type="QString" name="joinstyle"/>
                    <Option value="triangle" type="QString" name="name"/>
                    <Option value="0,0" type="QString" name="offset"/>
                    <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
                    <Option value="MM" type="QString" name="offset_unit"/>
                    <Option value="255,255,255,255" type="QString" name="outline_color"/>
                    <Option value="no" type="QString" name="outline_style"/>
                    <Option value="0.4" type="QString" name="outline_width"/>
                    <Option value="3x:0,0,0,0,0,0" type="QString" name="outline_width_map_unit_scale"/>
                    <Option value="MM" type="QString" name="outline_width_unit"/>
                    <Option value="area" type="QString" name="scale_method"/>
                    <Option value="1.8" type="QString" name="size"/>
                    <Option value="3x:0,0,0,0,0,0" type="QString" name="size_map_unit_scale"/>
                    <Option value="MM" type="QString" name="size_unit"/>
                    <Option value="1" type="QString" name="vertical_anchor_point"/>
                  </Option>
                  <effect type="effectStack" enabled="0">
                    <effect type="drawSource">
                      <Option type="Map">
                        <Option value="0" type="QString" name="blend_mode"/>
                        <Option value="2" type="QString" name="draw_mode"/>
                        <Option value="1" type="QString" name="enabled"/>
                        <Option value="1" type="QString" name="opacity"/>
                      </Option>
                    </effect>
                  </effect>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option value="" type="QString" name="name"/>
                      <Option name="properties"/>
                      <Option value="collection" type="QString" name="type"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </layer>
            <layer pass="0" id="{11cf8375-0ca7-41a1-9254-1503fa6606dc}" locked="0" enabled="1" class="MarkerLine">
              <Option type="Map">
                <Option value="4" type="QString" name="average_angle_length"/>
                <Option value="3x:0,0,0,0,0,0" type="QString" name="average_angle_map_unit_scale"/>
                <Option value="MM" type="QString" name="average_angle_unit"/>
                <Option value="3" type="QString" name="interval"/>
                <Option value="3x:0,0,0,0,0,0" type="QString" name="interval_map_unit_scale"/>
                <Option value="MM" type="QString" name="interval_unit"/>
                <Option value="0" type="QString" name="offset"/>
                <Option value="0" type="QString" name="offset_along_line"/>
                <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_along_line_map_unit_scale"/>
                <Option value="MM" type="QString" name="offset_along_line_unit"/>
                <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
                <Option value="MM" type="QString" name="offset_unit"/>
                <Option value="true" type="bool" name="place_on_every_part"/>
                <Option value="FirstVertex" type="QString" name="placements"/>
                <Option value="0" type="QString" name="ring_filter"/>
                <Option value="1" type="QString" name="rotate"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" type="QString" name="name"/>
                  <Option name="properties"/>
                  <Option value="collection" type="QString" name="type"/>
                </Option>
              </data_defined_properties>
              <symbol clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0" type="marker" name="@@0@0@2" is_animated="0">
                <data_defined_properties>
                  <Option type="Map">
                    <Option value="" type="QString" name="name"/>
                    <Option name="properties"/>
                    <Option value="collection" type="QString" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer pass="0" id="{41b403ba-c6f8-4c74-8e7c-d53173a13697}" locked="0" enabled="1" class="SimpleMarker">
                  <Option type="Map">
                    <Option value="0" type="QString" name="angle"/>
                    <Option value="square" type="QString" name="cap_style"/>
                    <Option value="0,0,0,255" type="QString" name="color"/>
                    <Option value="1" type="QString" name="horizontal_anchor_point"/>
                    <Option value="bevel" type="QString" name="joinstyle"/>
                    <Option value="square" type="QString" name="name"/>
                    <Option value="0,0" type="QString" name="offset"/>
                    <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
                    <Option value="MM" type="QString" name="offset_unit"/>
                    <Option value="0,0,0,255" type="QString" name="outline_color"/>
                    <Option value="solid" type="QString" name="outline_style"/>
                    <Option value="0.4" type="QString" name="outline_width"/>
                    <Option value="3x:0,0,0,0,0,0" type="QString" name="outline_width_map_unit_scale"/>
                    <Option value="MM" type="QString" name="outline_width_unit"/>
                    <Option value="area" type="QString" name="scale_method"/>
                    <Option value="1.6" type="QString" name="size"/>
                    <Option value="3x:0,0,0,0,0,0" type="QString" name="size_map_unit_scale"/>
                    <Option value="MM" type="QString" name="size_unit"/>
                    <Option value="1" type="QString" name="vertical_anchor_point"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option value="" type="QString" name="name"/>
                      <Option name="properties"/>
                      <Option value="collection" type="QString" name="type"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </layer>
          </symbol>
        </layer>
        <layer pass="0" id="{09f73ad8-b94a-4383-9b30-6e800530e34b}" locked="0" enabled="1" class="SimpleFill">
          <Option type="Map">
            <Option value="3x:0,0,0,0,0,0" type="QString" name="border_width_map_unit_scale"/>
            <Option value="0,0,0,255" type="QString" name="color"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="0,0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="35,35,35,255" type="QString" name="outline_color"/>
            <Option value="solid" type="QString" name="outline_style"/>
            <Option value="0.26" type="QString" name="outline_width"/>
            <Option value="MM" type="QString" name="outline_width_unit"/>
            <Option value="b_diagonal" type="QString" name="style"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <selection mode="Default">
    <selectionColor invalid="1"/>
    <selectionSymbol>
      <symbol clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0" type="fill" name="" is_animated="0">
        <data_defined_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </data_defined_properties>
        <layer pass="0" id="{7f013b6c-3a0b-4bb6-aed6-7099fc144cf5}" locked="0" enabled="1" class="SimpleFill">
          <Option type="Map">
            <Option value="3x:0,0,0,0,0,0" type="QString" name="border_width_map_unit_scale"/>
            <Option value="0,0,255,255" type="QString" name="color"/>
            <Option value="bevel" type="QString" name="joinstyle"/>
            <Option value="0,0" type="QString" name="offset"/>
            <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
            <Option value="MM" type="QString" name="offset_unit"/>
            <Option value="35,35,35,255" type="QString" name="outline_color"/>
            <Option value="solid" type="QString" name="outline_style"/>
            <Option value="0.26" type="QString" name="outline_width"/>
            <Option value="MM" type="QString" name="outline_width_unit"/>
            <Option value="solid" type="QString" name="style"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </selectionSymbol>
  </selection>
  <customproperties>
    <Option type="Map">
      <Option value="0" type="int" name="embeddedWidgets/count"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory enabled="0" lineSizeType="MM" height="15" spacingUnitScale="3x:0,0,0,0,0,0" showAxis="1" opacity="1" direction="0" width="15" penColor="#000000" minScaleDenominator="0" spacingUnit="MM" minimumSize="0" penWidth="0" rotationOffset="270" backgroundAlpha="255" spacing="5" backgroundColor="#ffffff" barWidth="5" scaleDependency="Area" scaleBasedVisibility="0" sizeScale="3x:0,0,0,0,0,0" maxScaleDenominator="1e+08" diagramOrientation="Up" sizeType="MM" penAlpha="255" lineSizeScale="3x:0,0,0,0,0,0" labelPlacementMethod="XHeight">
      <fontProperties italic="0" underline="0" bold="0" description="MS Shell Dlg 2,8.1,-1,5,50,0,0,0,0,0" strikethrough="0" style=""/>
      <axisSymbol>
        <symbol clip_to_extent="1" frame_rate="10" alpha="1" force_rhr="0" type="line" name="" is_animated="0">
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
          <layer pass="0" id="{8d78433f-e989-4802-9e4c-3996aaa07dac}" locked="0" enabled="1" class="SimpleLine">
            <Option type="Map">
              <Option value="0" type="QString" name="align_dash_pattern"/>
              <Option value="square" type="QString" name="capstyle"/>
              <Option value="5;2" type="QString" name="customdash"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="customdash_map_unit_scale"/>
              <Option value="MM" type="QString" name="customdash_unit"/>
              <Option value="0" type="QString" name="dash_pattern_offset"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="dash_pattern_offset_map_unit_scale"/>
              <Option value="MM" type="QString" name="dash_pattern_offset_unit"/>
              <Option value="0" type="QString" name="draw_inside_polygon"/>
              <Option value="bevel" type="QString" name="joinstyle"/>
              <Option value="35,35,35,255" type="QString" name="line_color"/>
              <Option value="solid" type="QString" name="line_style"/>
              <Option value="0.26" type="QString" name="line_width"/>
              <Option value="MM" type="QString" name="line_width_unit"/>
              <Option value="0" type="QString" name="offset"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="offset_map_unit_scale"/>
              <Option value="MM" type="QString" name="offset_unit"/>
              <Option value="0" type="QString" name="ring_filter"/>
              <Option value="0" type="QString" name="trim_distance_end"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_end_map_unit_scale"/>
              <Option value="MM" type="QString" name="trim_distance_end_unit"/>
              <Option value="0" type="QString" name="trim_distance_start"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="trim_distance_start_map_unit_scale"/>
              <Option value="MM" type="QString" name="trim_distance_start_unit"/>
              <Option value="0" type="QString" name="tweak_dash_pattern_on_corners"/>
              <Option value="0" type="QString" name="use_custom_dash"/>
              <Option value="3x:0,0,0,0,0,0" type="QString" name="width_map_unit_scale"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option value="" type="QString" name="name"/>
                <Option name="properties"/>
                <Option value="collection" type="QString" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings showAll="1" zIndex="0" dist="0" placement="1" obstacle="0" priority="0" linePlacementFlags="18">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option type="Map" name="QgsGeometryGapCheck">
        <Option value="0" type="double" name="allowedGapsBuffer"/>
        <Option value="false" type="bool" name="allowedGapsEnabled"/>
        <Option value="" type="QString" name="allowedGapsLayer"/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="NoFlag" name="fid">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="SUBCATCHMENTS.rain_gage">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="SUBCATCHMENTS.outlet">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="SUBCATCHMENTS.area">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="SUBCATCHMENTS.imperviousness">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="SUBCATCHMENTS.width">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="SUBCATCHMENTS.slope">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="SUBCATCHMENTS.curb_length">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="SUBCATCHMENTS.snow_pack">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="SUBAREAS.n_imperv">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="SUBAREAS.n_perv">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="SUBAREAS.storage_imperv">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="SUBAREAS.storage_perv">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="SUBAREAS.pct_zero">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="SUBAREAS.route_to">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="SUBAREAS.pct_routed">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="INFILTRATION.suction_head">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="INFILTRATION.hydraulic_conductivity">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="INFILTRATION.moisture_deficit_init">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="INFILTRATION.kind">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="tag">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" index="0" field="fid"/>
    <alias name="" index="1" field="name"/>
    <alias name="" index="2" field="SUBCATCHMENTS.rain_gage"/>
    <alias name="" index="3" field="SUBCATCHMENTS.outlet"/>
    <alias name="" index="4" field="SUBCATCHMENTS.area"/>
    <alias name="" index="5" field="SUBCATCHMENTS.imperviousness"/>
    <alias name="" index="6" field="SUBCATCHMENTS.width"/>
    <alias name="" index="7" field="SUBCATCHMENTS.slope"/>
    <alias name="" index="8" field="SUBCATCHMENTS.curb_length"/>
    <alias name="" index="9" field="SUBCATCHMENTS.snow_pack"/>
    <alias name="" index="10" field="SUBAREAS.n_imperv"/>
    <alias name="" index="11" field="SUBAREAS.n_perv"/>
    <alias name="" index="12" field="SUBAREAS.storage_imperv"/>
    <alias name="" index="13" field="SUBAREAS.storage_perv"/>
    <alias name="" index="14" field="SUBAREAS.pct_zero"/>
    <alias name="" index="15" field="SUBAREAS.route_to"/>
    <alias name="" index="16" field="SUBAREAS.pct_routed"/>
    <alias name="" index="17" field="INFILTRATION.suction_head"/>
    <alias name="" index="18" field="INFILTRATION.hydraulic_conductivity"/>
    <alias name="" index="19" field="INFILTRATION.moisture_deficit_init"/>
    <alias name="" index="20" field="INFILTRATION.kind"/>
    <alias name="" index="21" field="tag"/>
  </aliases>
  <splitPolicies>
    <policy policy="Duplicate" field="fid"/>
    <policy policy="Duplicate" field="name"/>
    <policy policy="Duplicate" field="SUBCATCHMENTS.rain_gage"/>
    <policy policy="Duplicate" field="SUBCATCHMENTS.outlet"/>
    <policy policy="Duplicate" field="SUBCATCHMENTS.area"/>
    <policy policy="Duplicate" field="SUBCATCHMENTS.imperviousness"/>
    <policy policy="Duplicate" field="SUBCATCHMENTS.width"/>
    <policy policy="Duplicate" field="SUBCATCHMENTS.slope"/>
    <policy policy="Duplicate" field="SUBCATCHMENTS.curb_length"/>
    <policy policy="Duplicate" field="SUBCATCHMENTS.snow_pack"/>
    <policy policy="Duplicate" field="SUBAREAS.n_imperv"/>
    <policy policy="Duplicate" field="SUBAREAS.n_perv"/>
    <policy policy="Duplicate" field="SUBAREAS.storage_imperv"/>
    <policy policy="Duplicate" field="SUBAREAS.storage_perv"/>
    <policy policy="Duplicate" field="SUBAREAS.pct_zero"/>
    <policy policy="Duplicate" field="SUBAREAS.route_to"/>
    <policy policy="Duplicate" field="SUBAREAS.pct_routed"/>
    <policy policy="Duplicate" field="INFILTRATION.suction_head"/>
    <policy policy="Duplicate" field="INFILTRATION.hydraulic_conductivity"/>
    <policy policy="Duplicate" field="INFILTRATION.moisture_deficit_init"/>
    <policy policy="Duplicate" field="INFILTRATION.kind"/>
    <policy policy="Duplicate" field="tag"/>
  </splitPolicies>
  <defaults>
    <default applyOnUpdate="0" expression="" field="fid"/>
    <default applyOnUpdate="0" expression="" field="name"/>
    <default applyOnUpdate="0" expression="" field="SUBCATCHMENTS.rain_gage"/>
    <default applyOnUpdate="0" expression="" field="SUBCATCHMENTS.outlet"/>
    <default applyOnUpdate="0" expression="" field="SUBCATCHMENTS.area"/>
    <default applyOnUpdate="0" expression="" field="SUBCATCHMENTS.imperviousness"/>
    <default applyOnUpdate="0" expression="" field="SUBCATCHMENTS.width"/>
    <default applyOnUpdate="0" expression="" field="SUBCATCHMENTS.slope"/>
    <default applyOnUpdate="0" expression="" field="SUBCATCHMENTS.curb_length"/>
    <default applyOnUpdate="0" expression="" field="SUBCATCHMENTS.snow_pack"/>
    <default applyOnUpdate="0" expression="" field="SUBAREAS.n_imperv"/>
    <default applyOnUpdate="0" expression="" field="SUBAREAS.n_perv"/>
    <default applyOnUpdate="0" expression="" field="SUBAREAS.storage_imperv"/>
    <default applyOnUpdate="0" expression="" field="SUBAREAS.storage_perv"/>
    <default applyOnUpdate="0" expression="" field="SUBAREAS.pct_zero"/>
    <default applyOnUpdate="0" expression="" field="SUBAREAS.route_to"/>
    <default applyOnUpdate="0" expression="" field="SUBAREAS.pct_routed"/>
    <default applyOnUpdate="0" expression="" field="INFILTRATION.suction_head"/>
    <default applyOnUpdate="0" expression="" field="INFILTRATION.hydraulic_conductivity"/>
    <default applyOnUpdate="0" expression="" field="INFILTRATION.moisture_deficit_init"/>
    <default applyOnUpdate="0" expression="" field="INFILTRATION.kind"/>
    <default applyOnUpdate="0" expression="" field="tag"/>
  </defaults>
  <constraints>
    <constraint unique_strength="1" exp_strength="0" constraints="3" notnull_strength="1" field="fid"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="name"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="SUBCATCHMENTS.rain_gage"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="SUBCATCHMENTS.outlet"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="SUBCATCHMENTS.area"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="SUBCATCHMENTS.imperviousness"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="SUBCATCHMENTS.width"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="SUBCATCHMENTS.slope"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="SUBCATCHMENTS.curb_length"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="SUBCATCHMENTS.snow_pack"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="SUBAREAS.n_imperv"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="SUBAREAS.n_perv"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="SUBAREAS.storage_imperv"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="SUBAREAS.storage_perv"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="SUBAREAS.pct_zero"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="SUBAREAS.route_to"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="SUBAREAS.pct_routed"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="INFILTRATION.suction_head"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="INFILTRATION.hydraulic_conductivity"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="INFILTRATION.moisture_deficit_init"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="INFILTRATION.kind"/>
    <constraint unique_strength="0" exp_strength="0" constraints="0" notnull_strength="0" field="tag"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="fid"/>
    <constraint desc="" exp="" field="name"/>
    <constraint desc="" exp="" field="SUBCATCHMENTS.rain_gage"/>
    <constraint desc="" exp="" field="SUBCATCHMENTS.outlet"/>
    <constraint desc="" exp="" field="SUBCATCHMENTS.area"/>
    <constraint desc="" exp="" field="SUBCATCHMENTS.imperviousness"/>
    <constraint desc="" exp="" field="SUBCATCHMENTS.width"/>
    <constraint desc="" exp="" field="SUBCATCHMENTS.slope"/>
    <constraint desc="" exp="" field="SUBCATCHMENTS.curb_length"/>
    <constraint desc="" exp="" field="SUBCATCHMENTS.snow_pack"/>
    <constraint desc="" exp="" field="SUBAREAS.n_imperv"/>
    <constraint desc="" exp="" field="SUBAREAS.n_perv"/>
    <constraint desc="" exp="" field="SUBAREAS.storage_imperv"/>
    <constraint desc="" exp="" field="SUBAREAS.storage_perv"/>
    <constraint desc="" exp="" field="SUBAREAS.pct_zero"/>
    <constraint desc="" exp="" field="SUBAREAS.route_to"/>
    <constraint desc="" exp="" field="SUBAREAS.pct_routed"/>
    <constraint desc="" exp="" field="INFILTRATION.suction_head"/>
    <constraint desc="" exp="" field="INFILTRATION.hydraulic_conductivity"/>
    <constraint desc="" exp="" field="INFILTRATION.moisture_deficit_init"/>
    <constraint desc="" exp="" field="INFILTRATION.kind"/>
    <constraint desc="" exp="" field="tag"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" sortExpression="" actionWidgetStyle="dropDown">
    <columns>
      <column hidden="0" type="field" name="fid" width="-1"/>
      <column hidden="0" type="field" name="name" width="-1"/>
      <column hidden="0" type="field" name="SUBCATCHMENTS.rain_gage" width="-1"/>
      <column hidden="0" type="field" name="SUBCATCHMENTS.outlet" width="-1"/>
      <column hidden="0" type="field" name="SUBCATCHMENTS.area" width="-1"/>
      <column hidden="0" type="field" name="SUBCATCHMENTS.imperviousness" width="-1"/>
      <column hidden="0" type="field" name="SUBCATCHMENTS.width" width="-1"/>
      <column hidden="0" type="field" name="SUBCATCHMENTS.slope" width="-1"/>
      <column hidden="0" type="field" name="SUBCATCHMENTS.curb_length" width="-1"/>
      <column hidden="0" type="field" name="SUBCATCHMENTS.snow_pack" width="-1"/>
      <column hidden="0" type="field" name="SUBAREAS.n_imperv" width="-1"/>
      <column hidden="0" type="field" name="SUBAREAS.n_perv" width="-1"/>
      <column hidden="0" type="field" name="SUBAREAS.storage_imperv" width="-1"/>
      <column hidden="0" type="field" name="SUBAREAS.storage_perv" width="-1"/>
      <column hidden="0" type="field" name="SUBAREAS.pct_zero" width="-1"/>
      <column hidden="0" type="field" name="SUBAREAS.route_to" width="-1"/>
      <column hidden="0" type="field" name="SUBAREAS.pct_routed" width="-1"/>
      <column hidden="0" type="field" name="INFILTRATION.suction_head" width="-1"/>
      <column hidden="0" type="field" name="INFILTRATION.hydraulic_conductivity" width="-1"/>
      <column hidden="0" type="field" name="INFILTRATION.moisture_deficit_init" width="-1"/>
      <column hidden="0" type="field" name="INFILTRATION.kind" width="-1"/>
      <column hidden="0" type="field" name="tag" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="INFILTRATION.hydraulic_conductivity"/>
    <field editable="1" name="INFILTRATION.kind"/>
    <field editable="1" name="INFILTRATION.moisture_deficit_init"/>
    <field editable="1" name="INFILTRATION.suction_head"/>
    <field editable="1" name="SUBAREAS.n_imperv"/>
    <field editable="1" name="SUBAREAS.n_perv"/>
    <field editable="1" name="SUBAREAS.pct_routed"/>
    <field editable="1" name="SUBAREAS.pct_zero"/>
    <field editable="1" name="SUBAREAS.route_to"/>
    <field editable="1" name="SUBAREAS.storage_imperv"/>
    <field editable="1" name="SUBAREAS.storage_perv"/>
    <field editable="1" name="SUBCATCHMENTS.area"/>
    <field editable="1" name="SUBCATCHMENTS.curb_length"/>
    <field editable="1" name="SUBCATCHMENTS.imperviousness"/>
    <field editable="1" name="SUBCATCHMENTS.outlet"/>
    <field editable="1" name="SUBCATCHMENTS.rain_gage"/>
    <field editable="1" name="SUBCATCHMENTS.slope"/>
    <field editable="1" name="SUBCATCHMENTS.snow_pack"/>
    <field editable="1" name="SUBCATCHMENTS.width"/>
    <field editable="1" name="fid"/>
    <field editable="1" name="name"/>
    <field editable="1" name="tag"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="INFILTRATION.hydraulic_conductivity"/>
    <field labelOnTop="0" name="INFILTRATION.kind"/>
    <field labelOnTop="0" name="INFILTRATION.moisture_deficit_init"/>
    <field labelOnTop="0" name="INFILTRATION.suction_head"/>
    <field labelOnTop="0" name="SUBAREAS.n_imperv"/>
    <field labelOnTop="0" name="SUBAREAS.n_perv"/>
    <field labelOnTop="0" name="SUBAREAS.pct_routed"/>
    <field labelOnTop="0" name="SUBAREAS.pct_zero"/>
    <field labelOnTop="0" name="SUBAREAS.route_to"/>
    <field labelOnTop="0" name="SUBAREAS.storage_imperv"/>
    <field labelOnTop="0" name="SUBAREAS.storage_perv"/>
    <field labelOnTop="0" name="SUBCATCHMENTS.area"/>
    <field labelOnTop="0" name="SUBCATCHMENTS.curb_length"/>
    <field labelOnTop="0" name="SUBCATCHMENTS.imperviousness"/>
    <field labelOnTop="0" name="SUBCATCHMENTS.outlet"/>
    <field labelOnTop="0" name="SUBCATCHMENTS.rain_gage"/>
    <field labelOnTop="0" name="SUBCATCHMENTS.slope"/>
    <field labelOnTop="0" name="SUBCATCHMENTS.snow_pack"/>
    <field labelOnTop="0" name="SUBCATCHMENTS.width"/>
    <field labelOnTop="0" name="fid"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="tag"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="INFILTRATION.hydraulic_conductivity" reuseLastValue="0"/>
    <field name="INFILTRATION.kind" reuseLastValue="0"/>
    <field name="INFILTRATION.moisture_deficit_init" reuseLastValue="0"/>
    <field name="INFILTRATION.suction_head" reuseLastValue="0"/>
    <field name="SUBAREAS.n_imperv" reuseLastValue="0"/>
    <field name="SUBAREAS.n_perv" reuseLastValue="0"/>
    <field name="SUBAREAS.pct_routed" reuseLastValue="0"/>
    <field name="SUBAREAS.pct_zero" reuseLastValue="0"/>
    <field name="SUBAREAS.route_to" reuseLastValue="0"/>
    <field name="SUBAREAS.storage_imperv" reuseLastValue="0"/>
    <field name="SUBAREAS.storage_perv" reuseLastValue="0"/>
    <field name="SUBCATCHMENTS.area" reuseLastValue="0"/>
    <field name="SUBCATCHMENTS.curb_length" reuseLastValue="0"/>
    <field name="SUBCATCHMENTS.imperviousness" reuseLastValue="0"/>
    <field name="SUBCATCHMENTS.outlet" reuseLastValue="0"/>
    <field name="SUBCATCHMENTS.rain_gage" reuseLastValue="0"/>
    <field name="SUBCATCHMENTS.slope" reuseLastValue="0"/>
    <field name="SUBCATCHMENTS.snow_pack" reuseLastValue="0"/>
    <field name="SUBCATCHMENTS.width" reuseLastValue="0"/>
    <field name="fid" reuseLastValue="0"/>
    <field name="name" reuseLastValue="0"/>
    <field name="tag" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"name"</previewExpression>
  <mapTip enabled="1"></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
