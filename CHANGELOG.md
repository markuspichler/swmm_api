# Changelog

<!--next-version-placeholder-->

## v0.4.64 (2025-02-17)

### Bug Fixes

* fix: removed default setting for (Coordinate Reference System) CRS in all functions (GIS export) and provided possibility to set system default of crs using the CONFIG object. (see readme for instruction) ([`f04e250`](https://gitlab.com/markuspichler/swmm_api/-/commit/f04e2504cdd641b0e31a0f0136062b7af888e6f5))


## v0.4.63 (2025-02-06)

### Bug Fixes

* fix: minor fix for arrows in map plot function ([`ad53d88`](https://gitlab.com/markuspichler/swmm_api/-/commit/ad53d88d7d3ab15e9c1ab15046be156730c2453a))

* fix: allow string and datetime for setting datetime in options section ([`515f8e8`](https://gitlab.com/markuspichler/swmm_api/-/commit/515f8e81ca773fcf0d6e174cad1ad7fc6c1b8f86))

### Documentation

* docs: added issues link to pypi ([`0f6e954`](https://gitlab.com/markuspichler/swmm_api/-/commit/0f6e95465dbe46a65e039bd53821518c68137781))

* docs: added contributing guide ([`de94446`](https://gitlab.com/markuspichler/swmm_api/-/commit/de944469c70af4b731ee13de699633ab8cabefd0))

* docs: cleanup of changelog ([`66ce733`](https://gitlab.com/markuspichler/swmm_api/-/commit/66ce73384e1a816ffcabf32612378b204943af6c))

### Testing

* test: using pytest instead of unittest ([`6b3a884`](https://gitlab.com/markuspichler/swmm_api/-/commit/6b3a884c86d1d1ea5979dc75fa2e7f7557521a8e))


## v0.4.62 (2025-01-14)

### Bug Fixes

* fix: added funktion to safe output data to parquet and hdf5 files using chunks for huge files. ([`cb0a85f`](https://gitlab.com/markuspichler/swmm_api/-/commit/cb0a85faec7fe05fe796fc43a9a63023374fb814))

* fix: added parameter color_nan for subcatchment map plot ([`4038481`](https://gitlab.com/markuspichler/swmm_api/-/commit/4038481f9a9584c1e8de344eb16d493426eb0d48))

* fix: added iternal input type helper functions ([`93d24d0`](https://gitlab.com/markuspichler/swmm_api/-/commit/93d24d01f1a6751824b1a4d2bf4e709b60e91457))

* fix: enhanced base section object comparison for input data comparison ([`4715ac5`](https://gitlab.com/markuspichler/swmm_api/-/commit/4715ac5ff2d43584674c4f39d235579007db990c))

* fix: converted LIDUsage.n_replicate to integer ([`88b69e0`](https://gitlab.com/markuspichler/swmm_api/-/commit/88b69e0eb0dd9f170823e69d81a2708d41189b2e))

* fix: fixed error with optional parameters in LID Usage ([`d4e4759`](https://gitlab.com/markuspichler/swmm_api/-/commit/d4e47590e76829be4474b5b31783137f3b8a00ab))

* fix: added set functions for temperature section ([`9a55b8e`](https://gitlab.com/markuspichler/swmm_api/-/commit/9a55b8e720c03090522800682a5b29957bb4f94e))

* fix: new global config option ([`02da0d3`](https://gitlab.com/markuspichler/swmm_api/-/commit/02da0d33b10898ff6a4ee783c2fb466d7059929b))

* fix: new global config option ([`5b74a6d`](https://gitlab.com/markuspichler/swmm_api/-/commit/5b74a6d2b2e86cb9479434683abc203d50ccb410))

* fix: fix error (wrong column assignment) for reading summary in report file with relative date column ([`b844a52`](https://gitlab.com/markuspichler/swmm_api/-/commit/b844a52e765cf890a96aad9122c253f60b30c3c4))

* fix: fix error (wrong column assignment) for reading LID Performance Summary in report file ([`662de50`](https://gitlab.com/markuspichler/swmm_api/-/commit/662de50a43d4995a3f32ca86fa0cd0599e9a366c))

* fix: change path to files in FILES section for temporary run ([`43110bf`](https://gitlab.com/markuspichler/swmm_api/-/commit/43110bf35ec1afae08b50ba2d785b42be93fb954))

* fix: added iternal input type helper functions ([`3f6640d`](https://gitlab.com/markuspichler/swmm_api/-/commit/3f6640d47cb8457d5ffb9be442a9929991924b6b))

* fix: added swmm5_run_temporary to top-level import ([`f6de9ba`](https://gitlab.com/markuspichler/swmm_api/-/commit/f6de9ba859098c63f7b55bd0ba6a1319ee6c4b07))

* fix: new global config option ([`2a8484c`](https://gitlab.com/markuspichler/swmm_api/-/commit/2a8484cae07fba5e351d8b26580e619a520da188))

### Code Style

* style: code style fixed ([`d7962ee`](https://gitlab.com/markuspichler/swmm_api/-/commit/d7962eea3696cf2ea60b1d6f566bab071204a06e))

* style: code style fixed ([`9fc62b5`](https://gitlab.com/markuspichler/swmm_api/-/commit/9fc62b53177b83901020dbed4c9def87ef4a25cc))

* style: code style fixed ([`41e70db`](https://gitlab.com/markuspichler/swmm_api/-/commit/41e70dbc7e354f0a22c607352e9b20e07df91933))

* style: code style fixed ([`f90e4ad`](https://gitlab.com/markuspichler/swmm_api/-/commit/f90e4adc0ccf12a08f74983ecee8ded0fb6e4f7c))

* style: changed fastparquet to pyarrow as optional dependency ([`38dadb6`](https://gitlab.com/markuspichler/swmm_api/-/commit/38dadb6dd23c62aa9ebd6c6db2caf8c347de32e2))

* style: code style fixed ([`dddee37`](https://gitlab.com/markuspichler/swmm_api/-/commit/dddee37bf4b06a18fed99a9de2e99b6c80a98fdd))

* style: updated QGIS style for GIS export ([`0240620`](https://gitlab.com/markuspichler/swmm_api/-/commit/024062094d6b992da1edbaa18c39a68ec0721211))

* style: using np.nan instead of importing nan from numpy ([`586ecf7`](https://gitlab.com/markuspichler/swmm_api/-/commit/586ecf73612e3aad4421db25243ea2595d760d54))

### Documentation

* docs: added path variable for examples ([`8a3174d`](https://gitlab.com/markuspichler/swmm_api/-/commit/8a3174d3d070324fca44ab5399460d41e6299d09))

* docs: cleanup changelog ([`b89e335`](https://gitlab.com/markuspichler/swmm_api/-/commit/b89e335220a6910da5a0d0a7646fe0e9168fd5f1))

* docs: removed CONFIG['exe_path'] due to set global config ([`4f86a99`](https://gitlab.com/markuspichler/swmm_api/-/commit/4f86a99684f051a3b4195f25f2a28a9b40e59684))

## v0.4.60 (2024-11-20)

### Bug Fixes

* fix: fixed behaviour when None was returned for iterating over empty sections ([`1ade6ec`](https://gitlab.com/markuspichler/swmm_api/-/commit/1ade6ec96b8cc19660932fcc54aae3429522d643))

* fix: return consistent empty section string for empty report section ([`d21e918`](https://gitlab.com/markuspichler/swmm_api/-/commit/d21e9181846447d015d4d21af4a981e1e6742b6e))

* fix: map plot - keep link label above link ([`2a36cc9`](https://gitlab.com/markuspichler/swmm_api/-/commit/2a36cc94a4a4a1a8433cc557bb7c9897de724e4c))

* fix: added comparison possibility (`==`) for options section ([`9cac7fb`](https://gitlab.com/markuspichler/swmm_api/-/commit/9cac7fb259f5d354085326bcee9cf031b6fb2630))

* fix: skip adding default value for empty evaporation section but keep it empty ([`84bcd66`](https://gitlab.com/markuspichler/swmm_api/-/commit/84bcd66b9a68fa25ee3adb3764979de5b0fec9f8))

* fix: return consistent empty section string for empty report section ([`41894e1`](https://gitlab.com/markuspichler/swmm_api/-/commit/41894e1439ad49abd4ed45ce6dd8e880fa7a3fd1))

* fix: prevent error for reading empty CONTROLS section ([`e9bfaf7`](https://gitlab.com/markuspichler/swmm_api/-/commit/e9bfaf7562b87273643e13592b3e079e862763e7))

* fix: hide warning for empty POLYGONS section ([`c945faa`](https://gitlab.com/markuspichler/swmm_api/-/commit/c945faa103485cac50430ae4bfc2474bcd872f49))

* fix: added kwargs in swmm5_run_temporary for swmm given runner ([`16f6d33`](https://gitlab.com/markuspichler/swmm_api/-/commit/16f6d3382b554d8062690a3749845a7cf3d414a5))

### Code Style

* style: better warning when adding object to section failed ([`c260643`](https://gitlab.com/markuspichler/swmm_api/-/commit/c260643ac462ea545a1695fc2edade6ac2679e99))

* style: out file to parquet rewritten ([`70af25c`](https://gitlab.com/markuspichler/swmm_api/-/commit/70af25c80a90c8f678128b01fa6e34c577c9f170))

* style: using class function instead of global function ([`5b82975`](https://gitlab.com/markuspichler/swmm_api/-/commit/5b82975b993b791e8045719447ec572b421cc0ff))

* style: code formatting ([`7e727b4`](https://gitlab.com/markuspichler/swmm_api/-/commit/7e727b487f2ea518cc1f87c96f4670966c6fb290))

### Documentation

* docs: minor file readme and code style ([`3e45114`](https://gitlab.com/markuspichler/swmm_api/-/commit/3e45114618c8bf3efa1e97e5854aa28acfaf4fc4))

* docs: minor readme and changelog polish ([`075d2ce`](https://gitlab.com/markuspichler/swmm_api/-/commit/075d2ce2904b477374f44173b003e3c6e15aaafa))

### Testing

* tests: added unittest for empty sections in input files ([`28a5cd3`](https://gitlab.com/markuspichler/swmm_api/-/commit/28a5cd3c72bd1e3b1be348aebac175592449cecf))


## v0.4.59 (2024-11-17)

### Documentation

- fixed type hints
- update of examples
- added docstrings
- str and Path possible for filenames
- updated examples + extended examples for documentation site

### Bug Fixes

- infiltration input data to string or file when fast=False and using mixed methods
- added convert args if list is given as string in gpkg_to_swmm function
- added better identification of infiltration method when model is added as geopackage
- added "+" and "+=" functionality to TitleSection
- added convert args if list is given as string (i.e. GIS import) for Storage and Outlet object
- inlet and timeseries cant be exported as pandas table because section can have different object types
- added convert args if list is given as string (i.e. GIS import)
- added timedelta object to types that cant be copied
- fixed warning when copying inp sections
- SwmmResults - skip reading lid report when "*" is used
- enhances in map plotting functions

### Code Style

- added infiltration type enum
- matplotlib syntax update
- pathlib instead of os.path
- use pandas infer_objects instead of fillna to fill None with nan
- simplified imports
- pathlib instead of os.path
- pathlib instead of os.path
- added tests for out file reader

**mics**

- update gitlab ci


## v0.4.58 (2024-11-12)

### Bug Fixes

* fix: skip convert object when object already in requested section ([`64120d5`](https://gitlab.com/markuspichler/swmm_api/-/commit/64120d530d6c0f0d189895a3face60a1e169b739))

### Code Style

* style: style removed fiona and Rtree as dependency ([`c15ec1e`](https://gitlab.com/markuspichler/swmm_api/-/commit/c15ec1eac8cf4a292f1a10f08a986e12ddc0be1a))

* style: added some predefined sections lists ([`20c2b94`](https://gitlab.com/markuspichler/swmm_api/-/commit/20c2b943428ff811f389de2612efa9f4b5025a9d))

* style: minor updates in code style and orders ([`1742379`](https://gitlab.com/markuspichler/swmm_api/-/commit/17423796a9a4c2e668a88299b789b44b7e8de718))

### Documentation

* docs: changed type hints ([`be8e67d`](https://gitlab.com/markuspichler/swmm_api/-/commit/be8e67d7f74f1b64c1f29e186e35ee55db599395))

* docs: minor updates in docs ([`8ecadc2`](https://gitlab.com/markuspichler/swmm_api/-/commit/8ecadc256bd0edbef48bf3563e939363ce1762fd))


## v0.4.57 (2024-11-07)

### Bug Fixes

* fix: complete required packages (forgot packaging-package) ([`9b1b70f`](https://gitlab.com/markuspichler/swmm_api/-/commit/9b1b70f5c24031bc92fb532e46b51468a9d3a5c6))

* fix: added macro to make map plot with bokeh ([`d4390d2`](https://gitlab.com/markuspichler/swmm_api/-/commit/d4390d23827ee7713f12a313e2d5f1fe66ca4b08))

* fix: added option to rasterize SC polygons in map plot ([`820fbb1`](https://gitlab.com/markuspichler/swmm_api/-/commit/820fbb197608e5667126576992ca8949b44424b0))

* fix: added option to add border-line around links in map ([`c5a203e`](https://gitlab.com/markuspichler/swmm_api/-/commit/c5a203ec0111eb0e31f2422dff66ccaccd6ed534))

### Code Style

* style: removed old snippets ([`66580fe`](https://gitlab.com/markuspichler/swmm_api/-/commit/66580fe258f7554f16465b9786ce46436d8350a7))

* style: updated GIS styles ([`96de324`](https://gitlab.com/markuspichler/swmm_api/-/commit/96de324739aca3b90d297c9bb02c84b5f9dc7cab))

* style: changed order of sections ([`0c56a6e`](https://gitlab.com/markuspichler/swmm_api/-/commit/0c56a6e385ed67116c3f30cf02828076fee107d6))

* style: minor efficiency fix ([`ea56e75`](https://gitlab.com/markuspichler/swmm_api/-/commit/ea56e7515634ce6423c70dd2d460f6509b815646))

### Documentation

* docs: added dependency tree in readme ([`c6eeafe`](https://gitlab.com/markuspichler/swmm_api/-/commit/c6eeafe1fc007dadcc346ede9d3081528949ede3))

* docs: dont add bokeh plot to docs ([`42cafbb`](https://gitlab.com/markuspichler/swmm_api/-/commit/42cafbb6e0f93dd70e2d93e16f3c9aee4baaa7a2))

* docs: dont add bokeh plot to docs ([`4ab7f20`](https://gitlab.com/markuspichler/swmm_api/-/commit/4ab7f204427a8f9ecae8f611a92ce3dfd0c7005d))

* docs: minor doc update ([`60897ed`](https://gitlab.com/markuspichler/swmm_api/-/commit/60897ed249575c4e3401bd6dafdd150c4a88ca5c))

### Unknown

* remove joss paper ([`2d3007d`](https://gitlab.com/markuspichler/swmm_api/-/commit/2d3007d4c77cf4ca5fdaa4feb409cd9d3142252c))

* Merge remote-tracking branch 'origin/main' ([`fe1274d`](https://gitlab.com/markuspichler/swmm_api/-/commit/fe1274d123baf4ece1fa5d002af5f39a56491cae))


## v0.4.56 (2024-10-29)

### Bug Fixes

* fix: added general convert object function ([`ebef9fa`](https://gitlab.com/markuspichler/swmm_api/-/commit/ebef9fa9076bb823d8d7ae8b8cc48dda05277a1f))

### Code Style

* style: renaming and moving of helpers functions ([`545bce7`](https://gitlab.com/markuspichler/swmm_api/-/commit/545bce7fc39f33747e33f47e60d4ed9d0b65fae9))

### Documentation

* docs: fixed error in type hint of docstring ([`d4b5e70`](https://gitlab.com/markuspichler/swmm_api/-/commit/d4b5e70186f6180ae6626516bf948fe6e54bc0ef))

* docs: example cleanup ([`79ea517`](https://gitlab.com/markuspichler/swmm_api/-/commit/79ea517dd0e9559d5b000b91fb382f00d6ae45e8))

### Unknown

* Merge remote-tracking branch 'origin/main' ([`787f3f8`](https://gitlab.com/markuspichler/swmm_api/-/commit/787f3f80598d9d318f23ca7664efb00167a39faf))


## v0.4.55 (2024-10-28)

### Bug Fixes

* fix: major map plot update ([`3509701`](https://gitlab.com/markuspichler/swmm_api/-/commit/3509701c01153c74849f28755a793abc6786cd34))

* fix: renamed write_calibration_files to write_calibration_file ([`76a7580`](https://gitlab.com/markuspichler/swmm_api/-/commit/76a75808e315d27da258214dce6b7ad7664f5e69))

* fix: added function swmm_api.external_files.read_calibration_file ([`d206d2b`](https://gitlab.com/markuspichler/swmm_api/-/commit/d206d2b265e41fde47e00af9d6872e98944fa73b))

* fix: added analysis tools for output timeseries. i.e. event analysis ([`6a3fa7b`](https://gitlab.com/markuspichler/swmm_api/-/commit/6a3fa7be8d30975f5298df5424ee799bb2e7bed5))

* fix: add swmm5_run_temporary to init level ([`87afebc`](https://gitlab.com/markuspichler/swmm_api/-/commit/87afebc74f44afddb33510250f87b3e4e04edd1a))

* fix: replace relative path with absolute path for temporary run ([`a664c9b`](https://gitlab.com/markuspichler/swmm_api/-/commit/a664c9b0d2af02a3cc7980583610570b382d01d4))

### Code Style

* style: using numpy abbr. np ([`04afa6f`](https://gitlab.com/markuspichler/swmm_api/-/commit/04afa6f8802275dc90fbb190f6be169d29822af2))

### Documentation

* docs: added jupyter notebook example for the first two epa swmm manual tutorials ([`e0caeb6`](https://gitlab.com/markuspichler/swmm_api/-/commit/e0caeb6464c4b37e0b8313fcca9065f3c782b8b9))

* docs: major map plot update in example ([`d5fc7bb`](https://gitlab.com/markuspichler/swmm_api/-/commit/d5fc7bb4a32cbd04f42cc8f17ae6dcb0569b9f36))

* docs: times-series example update ([`b570530`](https://gitlab.com/markuspichler/swmm_api/-/commit/b5705309769388deda133c5e8134cf622ce38520))

* docs: astlingen model example update ([`25af18b`](https://gitlab.com/markuspichler/swmm_api/-/commit/25af18b402eedcd63668c2bd2b12c9a0cc4b8d39))

* docs: added function header for function drop_useless_column_levels() ([`f87943f`](https://gitlab.com/markuspichler/swmm_api/-/commit/f87943f9f190e8412cbd2b501860625c07345505))

* docs: fixed typo ([`1f235e6`](https://gitlab.com/markuspichler/swmm_api/-/commit/1f235e6f861beffeb2218f6a9489d3eacf719519))

### Unknown

* Merge remote-tracking branch 'origin/main' ([`8e31694`](https://gitlab.com/markuspichler/swmm_api/-/commit/8e316948d365439172c02c110d189c818eca0a5e))


## v0.4.54 (2024-10-21)

### Bug Fixes

* fix: fixed typo in Hydrograph section object (MAY). Thanks to Zeda for the bug report. ([`5499fde`](https://gitlab.com/markuspichler/swmm_api/-/commit/5499fde95dd2baba3b23674e4f43689974829685))

### Documentation

* docs: docker update and CI update ([`8f04798`](https://gitlab.com/markuspichler/swmm_api/-/commit/8f0479889810459bc58c96c4e45def1f715df302))


## v0.4.53 (2024-10-21)

### Bug Fixes

* fix: added functions to plot network map using leaflet ([`53af029`](https://gitlab.com/markuspichler/swmm_api/-/commit/53af029b515acc25024d92a250cd90174018b845))

* fix: added functions to plot network map using plotly ([`d083127`](https://gitlab.com/markuspichler/swmm_api/-/commit/d083127d9f65401b602cd86039905058f990bd24))

* fix: added functions iter_avail_section_labels, iter_avail_sections, delete_section for SwmmInput ([`feb21db`](https://gitlab.com/markuspichler/swmm_api/-/commit/feb21dbdb23779ba82ddbf073870dc112a5b9b58))

* fix: added functions get_step, is_imperial, is_metric for OptionSection ([`4f6623f`](https://gitlab.com/markuspichler/swmm_api/-/commit/4f6623fd87209cec24f47eabd36bfd077a5430bf))

* fix: fixed wrong behaviour in function reduce_controls ([`d5a16df`](https://gitlab.com/markuspichler/swmm_api/-/commit/d5a16df8c6c01764fd7676f0136ad73308b0265b))

### Code Style

* style: delete_section as function of SwmmInput instead of macro ([`d93ecc6`](https://gitlab.com/markuspichler/swmm_api/-/commit/d93ecc65dd0f702587ef3474e1c197287198bb32))

* style: uniform import of shapely ([`a5aa091`](https://gitlab.com/markuspichler/swmm_api/-/commit/a5aa091ee7c68f5ad99500367b8d382f90f21efb))

### Documentation

* docs: added project that used swmm-api ([`db0c460`](https://gitlab.com/markuspichler/swmm_api/-/commit/db0c4603079bcd2224cb244652d2d9125a23e546))

* docs: added and enhanced some documentations ([`ed6f6b8`](https://gitlab.com/markuspichler/swmm_api/-/commit/ed6f6b81e1e68c9763966db9aa31c0c07d29472b))


## v0.4.52 (2024-10-11)

### Documentation

* docs: readme update ([`e7d3afd`](https://gitlab.com/markuspichler/swmm_api/-/commit/e7d3afda60f766104fedbfbec1bc8176dce464eb))

* docs: readme update ([`0eca851`](https://gitlab.com/markuspichler/swmm_api/-/commit/0eca851153fe7c01c678e5cdf99faf068d2af877))

* docs: added time slice option in the out file reader example ([`c70454a`](https://gitlab.com/markuspichler/swmm_api/-/commit/c70454a6a33670957726517525d790cbc9269bc1))

* docs: added get_part function in the readme ([`52f6b79`](https://gitlab.com/markuspichler/swmm_api/-/commit/52f6b796052be242cb6861c859d523fa50c3bc97))

### Fix

* fix: another ci release error ([`c3b37d9`](https://gitlab.com/markuspichler/swmm_api/-/commit/c3b37d93bf362f255c9c4ca050237fedf2faa87c))

* fix: check if SWMM exe is available ([`051d15b`](https://gitlab.com/markuspichler/swmm_api/-/commit/051d15b4c379e1a3291972b393b07b4f90798794))

* fix: fixed error when giving only start or end when only reading a slice of the out file. ([`17653b0`](https://gitlab.com/markuspichler/swmm_api/-/commit/17653b08e112f68a9ceed5bec02cea7b99f3457f))

* fix: TimeseriesData export error when giving a relative time using an integer ([`da85f29`](https://gitlab.com/markuspichler/swmm_api/-/commit/da85f290fef58533b362ab573a98292fbffce3c9))


## v0.4.51 (2024-10-02)

### Fix

* Fixed issue when more than 20 items were set in the REPORT section of the input-file ([`c14f294`](https://gitlab.com/markuspichler/swmm_api/-/commit/c14f294cbf096e0386c025d1f6516b5498b44917))

## v0.4.50 (2024-10-01)

### Fix

* Fixed issue when only a slice of the out-file is read ([`656aa99`](https://gitlab.com/markuspichler/swmm_api/-/commit/656aa9988c59b4ba8ea67383985f655ff6ec0242))

### Documentation

* Added column name example in report section docstring ([`81a2bef`](https://gitlab.com/markuspichler/swmm_api/-/commit/81a2bef20eec3bbca21e7f5471e7881a6dd7f840))
* Update example jupyter notebooks ([`a2b1584`](https://gitlab.com/markuspichler/swmm_api/-/commit/a2b15842228f0f8d6c43bcdfe70d0b26e2552bfa))
* Update example jupyter notebooks ([`92b3f99`](https://gitlab.com/markuspichler/swmm_api/-/commit/92b3f9903008d22f136c921d1a32e0c5bcfe8202))

## v0.4.49 (2024-10-01)

### Fix

* Fixed error when getting subcatchment dataframe in hotstart file ([`2a832be`](https://gitlab.com/markuspichler/swmm_api/-/commit/2a832befe25652ed79d8d5ff03006a682a8bc456))
* Skip reduced vertices when coordinates or vertices are missing ([`2a17a8a`](https://gitlab.com/markuspichler/swmm_api/-/commit/2a17a8a19e06f4813197ee38949c6e8e48ac89cb))
* Added parameter to create rasterized link map for plotting the model ([`7e2e655`](https://gitlab.com/markuspichler/swmm_api/-/commit/7e2e6552b40a874a9960f5f22cc70f8ee6422842))
* Added "to_file" alias function for SwmmInpu.write_file ([`4465abb`](https://gitlab.com/markuspichler/swmm_api/-/commit/4465abb607ea1f7891f097d4ed2051d54ff76eb9))
* Remove custom swmm lip for pyswmm run ([`8ddc466`](https://gitlab.com/markuspichler/swmm_api/-/commit/8ddc46674b42998a38105545578723a018ae2d7b))

### Documentation

* Update example jupyter notebooks ([`e889e9a`](https://gitlab.com/markuspichler/swmm_api/-/commit/e889e9a4266d6973f2c189f7992c367f6f8b6fd9))
* Extended custom swmm example ([`8dc992f`](https://gitlab.com/markuspichler/swmm_api/-/commit/8dc992fb222bf555ddd8632dc05667b6105a61f5))
* Added some examples to docstrings ([`4ccd48f`](https://gitlab.com/markuspichler/swmm_api/-/commit/4ccd48fba1b8616969e65c5f89222e6504d1b6bc))
* More publications using this package ([`58497f7`](https://gitlab.com/markuspichler/swmm_api/-/commit/58497f7b8332d1c0e92c7d3c9df8682402a19d32))

## v0.4.48 (2024-09-04)

### Fix

* Added warnung for function reduce_vertices() when end-node is not in inp-file (previously raised an error) ([`f030b24`](https://gitlab.com/markuspichler/swmm_api/-/commit/f030b242e600513a720e34fae524cacef237b03a))

### Documentation

* Update syntax in jupyter notebook ([`0af28ec`](https://gitlab.com/markuspichler/swmm_api/-/commit/0af28ec07853a18ecee7563129c2711167268210))
* Change in packages for doc-site ([`53fdab4`](https://gitlab.com/markuspichler/swmm_api/-/commit/53fdab491821193c7ef1a16d950d77319b29e24e))

## v0.4.47 (2024-08-23)

### Fix

* Fixed error in `swmm_api.input_file.macros.edit.rename_node` function where rename of outlet name of subcatchment failed [issue 16](https://gitlab.com/markuspichler/swmm_api/-/issues/16) ([`3d77ca5`](https://gitlab.com/markuspichler/swmm_api/-/commit/3d77ca5dc9da74c3fe1d8902eb3c8fa88b974db4))

### Documentation

* Cleanup changelog file ([`fe5d625`](https://gitlab.com/markuspichler/swmm_api/-/commit/fe5d625a397c6a500e4502c7a99ff95f6c3ecd6a))

## v0.4.46 (2024-08-22)

### Fix

* Changed behaviour of `SwmmInput.update` to always convert sections in inp-data as mentioned here [issue 15](https://gitlab.com/markuspichler/swmm_api/-/issues/15) ([`e19bffe`](https://gitlab.com/markuspichler/swmm_api/-/commit/e19bffee408076ea7f1e7f6a62a4f1f04a1b829d))
* Typo and simplification in string key as mentioned in [issue 17](https://gitlab.com/markuspichler/swmm_api/-/issues/17) ([`831e4b5`](https://gitlab.com/markuspichler/swmm_api/-/commit/831e4b55ed1ed1913de8d8dfe6281a9f59034f56))

## v0.4.45 (2024-08-05)

### Fix

* Swmm5_run_temporary SwmmResults class now has LID reports as attribute and sets report path into temporary folder ([`9f00292`](https://gitlab.com/markuspichler/swmm_api/-/commit/9f00292575618d676938de01dc1e0667397f2667))
* LID report now works with negative numbers ([`4f78a94`](https://gitlab.com/markuspichler/swmm_api/-/commit/4f78a941a0fc9e85c8c43ac61f40642b849794f1))

### Documentation

* Minor example update ([`f2c1544`](https://gitlab.com/markuspichler/swmm_api/-/commit/f2c1544dd54b3753287c05e7ffad1f84d9e7599b))

## v0.4.44 (2024-08-02)

### Fix

* Implemented ControlVariable and ControlExpression into function reduce_controls ([`ca7d73a`](https://gitlab.com/markuspichler/swmm_api/-/commit/ca7d73a08dc66686c924c911faefa033fa3d6e25))
* Implemented ControlVariable and ControlExpression into functions remove_obj_from_control and rename_obj_in_control_section ([`888e7ac`](https://gitlab.com/markuspichler/swmm_api/-/commit/888e7ac89804653db97805ce5d1a34fcb3c733b7))
* Added involves_variable_or_expression function to Control._Condition ([`e574e6f`](https://gitlab.com/markuspichler/swmm_api/-/commit/e574e6fdfff9d2741198b070c22a9bdb0731b9be))
* Added get_variables_involved function to ControlExpression ([`00be549`](https://gitlab.com/markuspichler/swmm_api/-/commit/00be549e2a88da581f02964129d7f08e0fa19b73))
* Renamed ControlAction attribute "variable" to "attribute" ([`1bcc4da`](https://gitlab.com/markuspichler/swmm_api/-/commit/1bcc4dac16a967b14c0ba00d6099763275644528))
* Renamed Control._Condition attribute from "kind" to "object_kind" ([`49edf6b`](https://gitlab.com/markuspichler/swmm_api/-/commit/49edf6b4ac9c89953a81f0fcb694e4eaaaae7e09))

### Documentation

* Added test scripts for control rule cleanup ([`e683ca8`](https://gitlab.com/markuspichler/swmm_api/-/commit/e683ca8b9553729b2e0f4918e91ef2e7b48c3fbd))

## v0.4.43 (2024-08-01)

### Fix

* Added possibility to set fixed set of pattern cycles for PatternRegression class ([`99095d5`](https://gitlab.com/markuspichler/swmm_api/-/commit/99095d5160da96a09379e5d2ecad3fbf3f9f69b1))
* Thanks to [Georges](https://gitlab.com/g_schutz) for pointing the error mentioned in [issue 13](https://gitlab.com/markuspichler/swmm_api/-/issues/13) where I used the wrong case for multiple comparisons ([`4f5c2c4`](https://gitlab.com/markuspichler/swmm_api/-/commit/4f5c2c44c5010f1e584e4f3324011b0eb67d9fc3))
* Fixed error in SuperConduit class if some sections are not in input data and enhanced some function names of class ([`870fb1a`](https://gitlab.com/markuspichler/swmm_api/-/commit/870fb1ade2cf123d25be554dbf8c0301785fc820))
* Adaption to new numpy version 2 ([`3d8904e`](https://gitlab.com/markuspichler/swmm_api/-/commit/3d8904e82435dfc73d7162889728da2270c0f1d8), [`da3c304`](https://gitlab.com/markuspichler/swmm_api/-/commit/da3c304a5e71d7529c987e8406e51a0b833e702d), [`c7687bf`](https://gitlab.com/markuspichler/swmm_api/-/commit/c7687bfcbe85034fb8a10202107b5668af784036), [`1f664dd`](https://gitlab.com/markuspichler/swmm_api/-/commit/1f664dd728e500044e1c997bd3a1bd731de94947), [`500ca67`](https://gitlab.com/markuspichler/swmm_api/-/commit/500ca671bb273844d3d5d06e6540e49a5fe13295))
* Added function convert_cms_to_lps (but still testing) ([`6836a96`](https://gitlab.com/markuspichler/swmm_api/-/commit/6836a9686be4811da77a1511d3095f276ba77936))
* Additional attributes for SwmmResults object, which is used in the swmm5_run_temporary class ([`1cde0f0`](https://gitlab.com/markuspichler/swmm_api/-/commit/1cde0f02bfea02cb86a51eef6a3c7df345107ad7))

### Documentation

* Added some swmm run examples ([`142d3c8`](https://gitlab.com/markuspichler/swmm_api/-/commit/142d3c821b634504cb3f15bf1d2deb4cdc1a9dfe))
* Updated astlingen jupyter notebook ([`7b618c2`](https://gitlab.com/markuspichler/swmm_api/-/commit/7b618c291618f2934d0652650d761c9dc9716805))
* Added additional text to LID docstring ([`d471f1d`](https://gitlab.com/markuspichler/swmm_api/-/commit/d471f1dee4e48892519ddea8134b0dca20bc08cb))

## v0.4.42 (2024-04-16)

### Fix

* Reading corrupt outfile raised an error. this is now fixed. ([`dc5e05e`](https://gitlab.com/markuspichler/swmm_api/-/commit/dc5e05e87815d468e2afe44ccb6e56998a757281))

### Documentation

* Added additional info for summary tables ([`3151964`](https://gitlab.com/markuspichler/swmm_api/-/commit/3151964b5f42cf9afb89b76544b60e6d23e6dcc1))
* Changed package description for pypi ([`62bd8f0`](https://gitlab.com/markuspichler/swmm_api/-/commit/62bd8f096abb37da11dfd26b2999ca7810f10f70))
* Fixed some links in doc-creation files ([`76720f4`](https://gitlab.com/markuspichler/swmm_api/-/commit/76720f4b88cfe7dadcbd2a88dfd5dff4779aa24c))

## v0.4.41 (2024-03-26)

### Fix

* Fixed error when empty polygon section is in inp-file. ([`1b48a33`](https://gitlab.com/markuspichler/swmm_api/-/commit/1b48a33ed28a4b32a51d0ae75c82465ed7554831))
* Added coefficient_curve as parameter for Weir object. ([`94810c1`](https://gitlab.com/markuspichler/swmm_api/-/commit/94810c1aad97b3181ae27eb06c89283030d8b6f0))
* Removed pandas deprecation-warning when using SwmmReport and pandas version > 2.2 ([`7872369`](https://gitlab.com/markuspichler/swmm_api/-/commit/787236928dee2551fda228eba57a911a72855641))
* Added return value to SwmmInput.read_text() ([`7080635`](https://gitlab.com/markuspichler/swmm_api/-/commit/7080635940820aeee4a18ab40214767fe88f9eee))
* Added function swmm_api.input_fie.macros.plotting_sub_map.plot_sub_map ([`38d9411`](https://gitlab.com/markuspichler/swmm_api/-/commit/38d9411dc73b52b040583d5b04bb79dc7d42f2f7))
* Added function plot_time_series_instabile_links ([`bf26de8`](https://gitlab.com/markuspichler/swmm_api/-/commit/bf26de806ace7c10f740941ce3a5b9e35154b73c))
* Moved climate-file and dat-timeseries-file functions to swmm_api.external_files ([`38e2085`](https://gitlab.com/markuspichler/swmm_api/-/commit/38e20853e8e921eaa17b9ae037efa5e4095b7014))
* Added WEIR as curve type in Curve class ([`aafaf43`](https://gitlab.com/markuspichler/swmm_api/-/commit/aafaf43c98978369f5e2e6aa334b84f64dbf5a35))
* Fixed treatment section string when using parameter SwmmInput.write_file(fast=False) ([`ad05bc6`](https://gitlab.com/markuspichler/swmm_api/-/commit/ad05bc6633c040c094509982ce63d772bd4a658c))
* Fixed error when empty polygon section is in inp-file. And fixed coverage section string when using parameter SwmmInput.write_file(fast=False) ([`cfe8cdf`](https://gitlab.com/markuspichler/swmm_api/-/commit/cfe8cdf4c8f16660539a07eb3ea3d256cb68e862))
* Fixed unexpected behaviour where several warnings are shown when using SwmmOutput.get_part(kind=None, ...) and one of label or variable is given. ([`139fa6f`](https://gitlab.com/markuspichler/swmm_api/-/commit/139fa6f73a28f2d7ea31358a93c86f1218e742fb))
* Added function print_raw_part to Swmm Report class ([`ef01593`](https://gitlab.com/markuspichler/swmm_api/-/commit/ef01593754a9465b88a725f060962d3cc897003d))

### Documentation

* Added some text to doc. ([`c263f50`](https://gitlab.com/markuspichler/swmm_api/-/commit/c263f50fee2f8d9f24a17300336f54761d33bed8))
* Cleanup of requirements info ([`b4ca60d`](https://gitlab.com/markuspichler/swmm_api/-/commit/b4ca60d3b3402273de3bfe4f1a33b563e9915c7c))
* Added warning in function create_sub_inp ([`53237d9`](https://gitlab.com/markuspichler/swmm_api/-/commit/53237d9bcd3512826888c6b9e6ed58ebf42c54cb))
* Changed some parameter value types in option section functions ([`82dbcea`](https://gitlab.com/markuspichler/swmm_api/-/commit/82dbceac434e676a0aae25bffb158d0d27f3535a))
* Fixed wrong link from pypi to changelog ([`0088d9f`](https://gitlab.com/markuspichler/swmm_api/-/commit/0088d9f83198af894e8552f5fa87d5aadc5c484f))
* Added master thesis where api was used ([`deccae2`](https://gitlab.com/markuspichler/swmm_api/-/commit/deccae23675270ffc380e9e0d0f83af24cdb658b))
* Added Matrix channel ([`e75ec4c`](https://gitlab.com/markuspichler/swmm_api/-/commit/e75ec4c2495d082dadfaa1c6d63cdb32391efb41))
* Added Matrix channel ([`07254e2`](https://gitlab.com/markuspichler/swmm_api/-/commit/07254e2b4c7d1f0bb29519aab7334cd3435ffcaf))
* Added citation information ([`fc0d230`](https://gitlab.com/markuspichler/swmm_api/-/commit/fc0d230311a72d94d7b6a8d19eb24f7183bd88d9))

## v0.4.40 (2024-02-27)

### Fix

* Fixed minor issue with mixed letter cases ([`a099f88`](https://gitlab.com/markuspichler/swmm_api/-/commit/a099f884f6a202169a1ebab78939b54c16f6f134))
* Fixed minor issue with control class ([`0772544`](https://gitlab.com/markuspichler/swmm_api/-/commit/0772544295a89835d022a4e8f10db16ea6e86391))
* Fixed issue when style table was not in geopackage when adding default style in apply_gis_style_to_gpkg function ([`9579535`](https://gitlab.com/markuspichler/swmm_api/-/commit/957953509fd553c1c695e9070ede7849b9d825ae))
* Added ffmpeg-path and inded-to-plot arguments to animated_plot_longitudinal function ([`e0e86d3`](https://gitlab.com/markuspichler/swmm_api/-/commit/e0e86d354bcbaf9bd2c4cc1b192c7654dc59019a))
* Added orifice_to_conduit macro function ([`083b9b6`](https://gitlab.com/markuspichler/swmm_api/-/commit/083b9b6903aab3aeec29b01709f39fd3bdf45fd8))

### Documentation

* Added master thesis where api was used ([`630585c`](https://gitlab.com/markuspichler/swmm_api/-/commit/630585cf82e63ccd44c510d01e40833fc15f0304))

## v0.4.39 (2024-02-13)

### Fix

* Added function to set default style for geopackage export when using QGIS ([`0592c17`](https://gitlab.com/markuspichler/swmm_api/-/commit/0592c17a3d1edabc4b27c44512f61d7f5e3052ba))

## v0.4.38 (2024-02-12)

### Fix

* Fixed issue in reduce_controls for new control syntax ([`70939ab`](https://gitlab.com/markuspichler/swmm_api/-/commit/70939abfbc608d52b11c79ef9ff5515db4431c03))
* Added function create_cut_model to cut a model and create timeseries dat files for cut links using a give out-file. ([`4d47bd0`](https://gitlab.com/markuspichler/swmm_api/-/commit/4d47bd0bf609433a8d70f2ae573732b6aa8a4a73))
* Fixed issue with string paths and pathlib paths in function compare_inp_files ([`d4ac897`](https://gitlab.com/markuspichler/swmm_api/-/commit/d4ac8972f440790076d96635eda1bc28e381eed1))
* Added new control syntax with variables and expressions. ([`da735c1`](https://gitlab.com/markuspichler/swmm_api/-/commit/da735c114a4c27fbf5167c7debbb6ca11059526e))
* Fixed issues with quoted strings in input file i.e. paths to files ([`3f01efe`](https://gitlab.com/markuspichler/swmm_api/-/commit/3f01efe0ef271ffda182d54e8384241202af8e91))
* Allow climate file with fewer columns. ([`638cfb6`](https://gitlab.com/markuspichler/swmm_api/-/commit/638cfb634a35d1c686bcb965ed4d055c2442acce))
* Don't show warning if section has multiple class-objects in it, when there are multiple objects allowed. ([`ba5bc1d`](https://gitlab.com/markuspichler/swmm_api/-/commit/ba5bc1dce06c6954d31a95b548cc27f63e28aca1))
* Implemented more elegant way to find last timestep in broken out-file ([`c792c29`](https://gitlab.com/markuspichler/swmm_api/-/commit/c792c2994a4b5ba9bfabc1cc80abac4132c8ef39))

### Documentation

* Added flow unit in comment ([`76994ab`](https://gitlab.com/markuspichler/swmm_api/-/commit/76994abca907b98266bea12a7704cc38dec0a2b5))
* Numbering publication in readme ([`b842526`](https://gitlab.com/markuspichler/swmm_api/-/commit/b8425268c93074464bd6523b4a333c444e5d2ada))

## v0.4.37 (2024-01-24)

### Fix

* Added ability to read specific time ranges from out file ([`ab2ee9f`](https://gitlab.com/markuspichler/swmm_api/-/commit/ab2ee9fb31e13c65e3501531d7a8d3da16b42d09))
* Added more info to print_summary(inp) ([`c7d9df7`](https://gitlab.com/markuspichler/swmm_api/-/commit/c7d9df79f80cf692e194877d16eba2f09f9b91a2))

### Documentation

* New citation ([`b80b39d`](https://gitlab.com/markuspichler/swmm_api/-/commit/b80b39df1260b3b7a83abed86d7d7f57bedd26c9))

## v0.4.36 (2024-01-11)

### Fix

* Fix error in delete_node function ([`9b7c83c`](https://gitlab.com/markuspichler/swmm_api/-/commit/9b7c83c1a7d0bed9fea1487829a6fbbd01aa1aab))

## v0.4.35 (2023-12-18)

### Fix

* Add subcatchment_geo_data_frame function to macros ([`8addde1`](https://gitlab.com/markuspichler/swmm_api/-/commit/8addde1e41712184108a68f6ee1023c660fb39fb))

## v0.4.34 (2023-12-07)

### Fix

* SwmmOutput - save model parameters in parquet file ([`c39e82f`](https://gitlab.com/markuspichler/swmm_api/-/commit/c39e82f8a807600bc8ed4692e42b5e017597522d))
* Read options section in report-file + warning for empty file ([`64abc8d`](https://gitlab.com/markuspichler/swmm_api/-/commit/64abc8d9266c5892545c5bdbd111a62085d56fd3))
* Read inp/rpt file from provided io-buffer ([`d8fced0`](https://gitlab.com/markuspichler/swmm_api/-/commit/d8fced0e8d2e44f6dd34927da50cdbeaeb991113))

## v0.4.33 (2023-12-01)

### Fix

* Copy None values fixed ([`3aedafe`](https://gitlab.com/markuspichler/swmm_api/-/commit/3aedafe2f8561196e93a68bc7007b8476655bdcf))

## v0.4.32 (2023-11-28)

### Fix

* Error with SwmmOutput.to_parquet() ([`213cd52`](https://gitlab.com/markuspichler/swmm_api/-/commit/213cd52fdadf1be489eb31cd56cfde4e16eab93a))

### Documentation

* Additional info in docstrings ([`93ad3aa`](https://gitlab.com/markuspichler/swmm_api/-/commit/93ad3aaaba27692d47bd3205e14a019ef5fed02f))

## v0.4.31 (2023-11-17)

### Fix

* Error in new reduce_controls() function ([`d1ffa98`](https://gitlab.com/markuspichler/swmm_api/-/commit/d1ffa9803413036594b6349806f21b8a58a3473e))

## v0.4.30 (2023-10-30)

### Fix

* Added macros function remove_obj_from_control and rename_obj_in_control_section and implemented them in delete_node/_links and rename_node/_links ([`11fd554`](https://gitlab.com/markuspichler/swmm_api/-/commit/11fd55443a7603fa41157d00d80eadd2b06e15e9))
* Added new types and attributes in Control.OBJECTS and Control.ATTRIBUTES (new in SWMM 5.2) ([`0834774`](https://gitlab.com/markuspichler/swmm_api/-/commit/08347744c7dded37509caca95892e420b63cdb8c))
* Fixed error in reduce_controls macro function ([`d86cd12`](https://gitlab.com/markuspichler/swmm_api/-/commit/d86cd12b845e74685b090972636d19851f030e35))

## v0.4.29 (2023-10-27)

### Fix

* Added reduce_hydrographs, reduce_snowpacks and reduce_report_objects to macros.reduce_unneeded ([`0a812fa`](https://gitlab.com/markuspichler/swmm_api/-/commit/0a812fad28f7a075d2c33358e75454d7f1c07b11))

## v0.4.28 (2023-10-19)

### Fix

* Added a warning when reading a very large output-file. ([`8b813c8`](https://gitlab.com/markuspichler/swmm_api/-/commit/8b813c809dab7e195c6a497b637be98b44ce4e6c))

## v0.4.27 (2023-10-17)

### Fix

* Resolved issues when running an input file using a relative path. ([`2c4b260`](https://gitlab.com/markuspichler/swmm_api/-/commit/2c4b260cd5c050fd4bb466c138d0e3702b2a2e90))

### Documentation

* Added additional links in docs of run_swmm ([`e19f7cf`](https://gitlab.com/markuspichler/swmm_api/-/commit/e19f7cfadf692440015d42a64e49bcfdf571ac51))
* Update readme ([`14105b0`](https://gitlab.com/markuspichler/swmm_api/-/commit/14105b0a5d8ef97e11fab59f70e2322ed7aaadf2))
* Function header of subcatchments_connected ([`fd5468c`](https://gitlab.com/markuspichler/swmm_api/-/commit/fd5468c53e36c536a2530dfa1abd3a19014bdd55))

## v0.4.26 (2023-08-10)

### Fix

* Set custom working directory for running swmm. new default is directory of the input-file. ([`b472986`](https://gitlab.com/markuspichler/swmm_api/-/commit/b4729863986fd21ecb9d51c0fce3b8fa65c6d0e1))

## v0.4.25 (2023-08-03)

### Fix

* Update gis import to remove depreciated function ([`2a38e2e`](https://gitlab.com/markuspichler/swmm_api/-/commit/2a38e2e6e472f3835c31844337d89a8466370930))

## v0.4.24 (2023-07-31)

### Fix

* Conduits_are_equal returns false if  both links are not the same type ([`e219ac6`](https://gitlab.com/markuspichler/swmm_api/-/commit/e219ac6f197bfa4c3de2a194bb7f24640782a999))

## v0.4.23 (2023-07-21)

### Fix

* Added reader for LID report file ([`546694b`](https://gitlab.com/markuspichler/swmm_api/-/commit/546694b7a47a34d18384852579adb354c275adec))

### Documentation

* Icons on doc site ([`bc8c815`](https://gitlab.com/markuspichler/swmm_api/-/commit/bc8c815f0b78ca39e2736f3a70532f5f83546905))
* Update dependencies due to deprecations ([`8c8d343`](https://gitlab.com/markuspichler/swmm_api/-/commit/8c8d3433ba71263631708e182b19d1d0047ddb72))
* New autolinks in jupyter notebook code ([`d4e0b60`](https://gitlab.com/markuspichler/swmm_api/-/commit/d4e0b60279b82fae201bb0105452e28137e30bca))
* New autolinks in jupyter notebook code ([`28f4229`](https://gitlab.com/markuspichler/swmm_api/-/commit/28f42295a258a6c0658a2c2d5994eba1777b942b))

## v0.4.22 (2023-06-07)
### Fix
* Added function for adding layers to LIDcontrol ([`d31a3d2`](https://gitlab.com/markuspichler/swmm_api/-/commit/d31a3d296325b02c01255fb790f18f7e2289cc05))
* fixed type hinting for inp-sections with tuples as keys
* removed lid_kind as identifier of LIDControl object.

### Documentation
* added example for LIDs

## v0.4.21 (2023-05-04)
### Fix
* Set back to default locale in timeseries data converter ([`56d1af7`](https://gitlab.com/markuspichler/swmm_api/-/commit/56d1af71beb4a704a72c5ff9963c98498d38a6e5))
* Optional show progressbar in compare_inp_files ([`d2661f2`](https://gitlab.com/markuspichler/swmm_api/-/commit/d2661f2aabd6cd826a28370881ddc3ffbe8dde0c))

## v0.4.20 (2023-04-17)
### Fix
* Fixed errors in the `Control` object implementation. `actions` attribute is now split into `actions_if` and `actions_else`. Actions don't need the parameter `logic`. ([`2aa71a9`](https://gitlab.com/markuspichler/swmm_api/-/commit/2aa71a90427e978baf8af0736f065bc908fa2062))
* deeper copy for controls. If a copy of inp uses the reduce_controls, actions got lost for the original inp to, now it got fixed.
* 
## v0.4.19 (2023-04-13)
### Fix
* Weir road surface parameter not used, was a string, now its a nan and will not be written into the new inp file. ([`25d665b`](https://gitlab.com/markuspichler/swmm_api/-/commit/25d665bb46a2bfc4c82a4446bc590f3b92a58f88))
* renamed downstream_nodes to get_downstream_nodes
* renamed upstream_nodes to get_upstream_nodes
* don't convert sections when deleting empty sections

## v0.4.18 (2023-04-08)
### Fix
* Raise error when swmm run failed with owa swmm ([`0aa9743`](https://gitlab.com/markuspichler/swmm_api/-/commit/0aa97438c32bcdb20358ebf8bc16dc5b7ff970b4))
* Optimize polygon import for large models ([`52d1f53`](https://gitlab.com/markuspichler/swmm_api/-/commit/52d1f53c0bd5d99ce8a0bbf5a0006c2e6099b87f))

### Documentation
* Extended the testing swmm inp model ([`39c08f2`](https://gitlab.com/markuspichler/swmm_api/-/commit/39c08f23a72c6bf538dc030fb017b92a6d75ea1d))
* Update out file example ([`cdbce04`](https://gitlab.com/markuspichler/swmm_api/-/commit/cdbce04384007dc6dba8d532fa0914c0d53360b0))
* Update out file example ([`d746ce3`](https://gitlab.com/markuspichler/swmm_api/-/commit/d746ce39bab919273b42390e635d265febd504fb))

## v0.4.17 (2023-04-06)
### Fix
* Added rainfall_dependent_ii, street_flow_summary, shape_summary and street_summary as property to the SwmmReport class ([`70ff5aa`](https://gitlab.com/markuspichler/swmm_api/-/commit/70ff5aa3b044a670447d1d8d754d65c8a062bbaf))

## v0.4.16 (2023-04-04)
### Fix
* Added to_parquet_chunks for SwmmOutput to write parquet files in chunks to prevent out of memory error for huge out files. ([`6ac200b`](https://gitlab.com/markuspichler/swmm_api/-/commit/6ac200b74b51f216d564adffaae3e8f52e994e66))

### Documentation
* Polygon size is limited in gui ([`68b84dc`](https://gitlab.com/markuspichler/swmm_api/-/commit/68b84dc20bd54bcef6ef97575845f5948f81e270))

## v0.4.15 (2023-03-28)
### Fix
* Run swmm in temporary folder and get results ([`e79b355`](https://gitlab.com/markuspichler/swmm_api/-/commit/e79b355bff1732022e1aa3d36a8fa230ed66790a))

## v0.4.14 (2023-03-27)
### Fix
* Enhanced swmm exe search ([`1401b56`](https://gitlab.com/markuspichler/swmm_api/-/commit/1401b568599beaee9bfdae6ccd72089ed9c92f27))
* Error due to locale in combine dw-flows ([`d118b3b`](https://gitlab.com/markuspichler/swmm_api/-/commit/d118b3be4e5e3dfe91b857231990d5ad4ce49d0f))

## v0.4.13 (2023-03-27)
### Fix
* Finding correct swmm executable ([`3a8301f`](https://gitlab.com/markuspichler/swmm_api/-/commit/3a8301f713c789985ff918cf19924ea91c1b4569))
* Setting locale with macOS for timeseries date format conversion ([`e55c1ff`](https://gitlab.com/markuspichler/swmm_api/-/commit/e55c1ff86752591f4b1cf1b3453a39d3c30f13c0))

## v0.4.12 (2023-03-24)
### Fix
* Input_file.macros.move_flows now takes the pattern of the dominant node ([`bf6612a`](https://gitlab.com/markuspichler/swmm_api/-/commit/bf6612a98f400346017a266fe7dcd699448c7eee))

## v0.4.11 (2023-03-14)
### Fix
* Timeseries conversion when locale is not english ([`1cd934e`](https://gitlab.com/markuspichler/swmm_api/-/commit/1cd934ef29507112d93ee2feeee78c7b9f3e0c23))
* Wrong identifies in Groundwater object ([`347b0a2`](https://gitlab.com/markuspichler/swmm_api/-/commit/347b0a20f922540d5a2e727b4d6c7198ddd63c77))
* Added function swmm_api.input_file.macros.get_downstream_path ([`dca930b`](https://gitlab.com/markuspichler/swmm_api/-/commit/dca930b7a7906e08c8fe5b6d962d2174d6be99b5))

### Documentation
* Extended gis to swmm example ([`4051127`](https://gitlab.com/markuspichler/swmm_api/-/commit/4051127550eff2c750e2c6a26d0a36e6125678af))
* Swmm5_run_epa function: paths as string or Path ([`0e7eceb`](https://gitlab.com/markuspichler/swmm_api/-/commit/0e7eceb91a250c094c2987c6b4bceccf88210d01))
* Extended gis to swmm example ([`202dab8`](https://gitlab.com/markuspichler/swmm_api/-/commit/202dab87e478f89719deff861c622ae1e2d86c21))

## v0.4.10 (2023-03-13)
### Fix
* Error with function SwmmInput.check_for_section [Issue #6](https://gitlab.com/markuspichler/swmm_api/-/issues/6) ([`ddeb730`](https://gitlab.com/markuspichler/swmm_api/-/commit/ddeb730d4782b999653c445e3f39e6a638c3a50f))

## v0.4.9 (2023-03-09)
### Fix
* Added option to not show progressbar in selective out-file reader ([`ea8b666`](https://gitlab.com/markuspichler/swmm_api/-/commit/ea8b6667f1ebb8944f10db9056e19f90a3e86910))

## v0.4.8 (2023-03-09)
### Fix
* Added InpSectionDummy and DummySectionObject classes for unknown sections in the inp-file ([`1949f6a`](https://gitlab.com/markuspichler/swmm_api/-/commit/1949f6ac43e4e445e2e229c78aba64562f3115bc))
* Added setter for OPTIONS section ([`d361916`](https://gitlab.com/markuspichler/swmm_api/-/commit/d361916f5249da5dd32ad74ec5d12d7e647eb72c))
* Inlet object was broken in the reader ([`9a08149`](https://gitlab.com/markuspichler/swmm_api/-/commit/9a08149c1565a68055a2b84c49c9b5661f7dc6e5))
* Added swmm-version to CONFIG for syntax differences in inp-file creation ([`8c08851`](https://gitlab.com/markuspichler/swmm_api/-/commit/8c0885109eda84e31b5664a5a633b2f82b4d8a63))

### Documentation
* Added example for custom inp section ([`f797dac`](https://gitlab.com/markuspichler/swmm_api/-/commit/f797dac6bbe25018c264a81ef2fc357db8be8110))

## v0.4.7 (2023-03-08)
### Fix
* Added lost swmm 5.2 sections to input converter ([`7249f72`](https://gitlab.com/markuspichler/swmm_api/-/commit/7249f72eeb6eb934fc53ad760818feaf9243c5aa))

## v0.4.6 (2023-03-07)
### Fix
* Added config for path to default swmm exe ([`2807d8f`](https://gitlab.com/markuspichler/swmm_api/-/commit/2807d8f68842fcddbd397614c39411dee088b81d))
* Raise FileNotFoundError when file is not available ([`4ae6dd1`](https://gitlab.com/markuspichler/swmm_api/-/commit/4ae6dd15f733051f7099309445298b616f9e9967))

## v0.4.5 (2023-03-05)
### Fix
* Add gis_decimals to swmm-api-config ([`c85aeba`](https://gitlab.com/markuspichler/swmm_api/-/commit/c85aeba364bc14ff730c01461b1478a01a829904))

### Documentation
* Fixed doc creation ([`d32cde3`](https://gitlab.com/markuspichler/swmm_api/-/commit/d32cde3184e098055a025b55d03932e561199050))

## v0.4.4 (2023-03-03)
### Fix
* Added macro `subcatchments_connected` ([`8fa7dc9`](https://gitlab.com/markuspichler/swmm_api/-/commit/8fa7dc94e96a97f940fc8fd3769b288547c48982))

### Documentation
* Minor doc fixes ([`081205d`](https://gitlab.com/markuspichler/swmm_api/-/commit/081205d454901b5e2a41d0c6ee2e22a91ea047d6))
* Minor doc fixes ([`059902c`](https://gitlab.com/markuspichler/swmm_api/-/commit/059902c5d0fa06b73354354958fd87ca1706d027))
* Enhanced doc build ([`ff09a2d`](https://gitlab.com/markuspichler/swmm_api/-/commit/ff09a2deef68f94590675f84e92ae75af09e50df))

## v0.4.3 (2023-02-24)
### Fix
* Added setter to OptionSection ([`3d927ce`](https://gitlab.com/markuspichler/swmm_api/-/commit/3d927cec2e85333e36fea6a20986b67f491aec79))
* Fixed wrong outfile data index ([`34f7350`](https://gitlab.com/markuspichler/swmm_api/-/commit/34f735034d694af54b3f6580271a727080995526))
* Fixed wrong outfile data index ([`aebc9e5`](https://gitlab.com/markuspichler/swmm_api/-/commit/aebc9e50993ee1b76e58b5d8dc18982e217b2095))
* Added function write_calibration_files ([`b2f52e6`](https://gitlab.com/markuspichler/swmm_api/-/commit/b2f52e6d93b32a26bfe235376d555c6b8c60306c))
* Added function animated_plot_longitudinal ([`f19915e`](https://gitlab.com/markuspichler/swmm_api/-/commit/f19915ebcd7df9a7eba5b27cc2c84486950231ab))

### Documentation
* Inp write file also possible as Path-type ([`f327d55`](https://gitlab.com/markuspichler/swmm_api/-/commit/f327d557d31d6b74f4825d6600856bc937880c32))

## v0.4.2 (2023-02-15)
### Fix
* Input-file class copy function also copies the converter classes, the encoding and the section ordering ([`03ad15b`](https://gitlab.com/markuspichler/swmm_api/-/commit/03ad15b9eb742609dbd308dacf35604eaadfb905))
* Owa-swmm-api only takes string for the input-filename ([`31ebf60`](https://gitlab.com/markuspichler/swmm_api/-/commit/31ebf603ac22bce95c8b755ed4e049d374bc2d9a))

## v0.4.1 (2023-02-13)
### Fix
* Inp_to_graph adds now the swmm-objects to the graph object ([`444fb4f`](https://gitlab.com/markuspichler/swmm_api/-/commit/444fb4fb3957bab2964aed12898ec855d1f00ccd))
* Improvements for delete_node(), move_node(), reconnect_subcatchments(), combine_conduits() ([`9a81ba9`](https://gitlab.com/markuspichler/swmm_api/-/commit/9a81ba9216b36c49bc784f2d7654a25cfb961dd1))
* Remove empty sections before gis export ([`6a244c7`](https://gitlab.com/markuspichler/swmm_api/-/commit/6a244c73fd0cba47dbfc557ef2c69e6f93ce1738))
* Get_result_filename accepts now a `Path` as parameter ([`6f78982`](https://gitlab.com/markuspichler/swmm_api/-/commit/6f78982711b44dfba3d84df4926d0acbf90efe6a))
* Pyswmm only takes string for the input-filename ([`82603c7`](https://gitlab.com/markuspichler/swmm_api/-/commit/82603c7a57b15041692bfc1176816871239156f3))

### Documentation
* Allow path and string ([`ff62215`](https://gitlab.com/markuspichler/swmm_api/-/commit/ff6221565add779d1cf0eced9fd4d06b5dbefa43))
* Minor documentation fixes ([`adf4d9b`](https://gitlab.com/markuspichler/swmm_api/-/commit/adf4d9bf9aa8a71baa3219f896d24c9c02588292))

## v0.4.0 (2023-02-10)
### Feature
* Adding warning to out-file-reader when request is not found. ([`baaaa58`](https://gitlab.com/markuspichler/swmm_api/-/commit/baaaa583832a462d9f004d538a8928cd364b924b))
* Ability to set global default encoding for reading inp, out, and report files. ([`8365ba8`](https://gitlab.com/markuspichler/swmm_api/-/commit/8365ba85dd272fd229c8dc53da867c9000efdde5))

### Documentation
* New docker image for the website generation ([`6333ccc`](https://gitlab.com/markuspichler/swmm_api/-/commit/6333ccca6fbd7d6f4286e7fc574a687936b00308))
* Into + joss-paper ([`2bacb27`](https://gitlab.com/markuspichler/swmm_api/-/commit/2bacb279c1ca6cb2b51f0b04f8b1a4f238b5c26c))
* Typo ([`2f35161`](https://gitlab.com/markuspichler/swmm_api/-/commit/2f35161aeb42dc68a5af2c76aff8495351e53826))
* Typo ([`cbc9f3a`](https://gitlab.com/markuspichler/swmm_api/-/commit/cbc9f3a824aa41b98f44ade7d4e705a53126aee5))
* Changes in readme ([`3a240b6`](https://gitlab.com/markuspichler/swmm_api/-/commit/3a240b6440fbae9f87d5fad97a0ce2f24af988e6))
* Changes in readme ([`cea2da5`](https://gitlab.com/markuspichler/swmm_api/-/commit/cea2da5fc3bc43cf5855cf1b8e458bdb6a217c28))
* Changes in readme ([`9b069dd`](https://gitlab.com/markuspichler/swmm_api/-/commit/9b069ddf966f7ad3e22f32cfade10a1b658369e1))
* Added reference to readme ([`6da8f41`](https://gitlab.com/markuspichler/swmm_api/-/commit/6da8f411fd90d51eccecb5386365f9977bdaabd4))
* Typo in readme ([`255132f`](https://gitlab.com/markuspichler/swmm_api/-/commit/255132f92eefc6b2f35c191654a09ba8a73aa061))

## v0.3.3 (2023-02-03)

### Fix

* Added keyword arguments to control condition and action init ([`bf1ad7a`](https://gitlab.com/markuspichler/swmm_api/-/commit/bf1ad7a166002e2562831f64332c12a8075b7ace))

### Documentation

* Tutorial How to add control rules ([`0ca1d9c`](https://gitlab.com/markuspichler/swmm_api/-/commit/0ca1d9cef327c18d5447af8db977b179c1310668), [`b8d652f`](https://gitlab.com/markuspichler/swmm_api/-/commit/b8d652f02402f56d9cf0c3948014bf998296b076), [`9442147`](https://gitlab.com/markuspichler/swmm_api/-/commit/9442147211cbed03fbb45a3a6b0489a317ed8659))
* Adding example to import gis data ([`e2c148e`](https://gitlab.com/markuspichler/swmm_api/-/commit/e2c148e5eb35dbf93ddc759aed6b325cb49eb66c))
* Adding publications mentioning swmm-api to readme ([`4d209ab`](https://gitlab.com/markuspichler/swmm_api/-/commit/4d209aba439ae3d1cc55bcbe6b5613743441ac6d))

### Style

* matplotlib constrained layout

## v0.3.2 (2023-01-09)

### Fix

* Added ability to set * as offset for "offset measured as elevation"-option. ([`6994640`](https://gitlab.com/markuspichler/swmm_api/-/commit/699464091dacfc67283a7ea7b6a84bee8d105e8f))
* case insensitive string comparison for type convertion of "YES", "NO" and "NONE"
* infer_offset_elevation function (only internal)

### Documentation

* added warning in readme
* added some icons to headers
* capital letter at beginning

## v0.3.1 (2022-12-16)

### Fix

* Ci-test ([`189d64c`](https://gitlab.com/markuspichler/swmm_api/-/commit/189d64c958426679339cfb781b7fd64755647931))

### Added

* get_geo_length to Vertices object
* possibility to set path as a pathlib.Path-object
* parameter for minimum length to simplify vertices function 

## 0.3.post3 (Nov 21, 2022)

### Fix

- HotstartFileReader fixed if not all sections in .inp-file

## 0.3.post2 (Oct 18, 2022)

### Fix

- added needed package data-file

## 0.3.post1 (Oct 17, 2022)

### Changed

- running swmm functions are reordered
- internal package structure

### Added

- reading .rpt-file with encoding

## 0.3 (Sep 06, 2022)

### Removed

- CoordinatesGeo (functionality included in Coordinate)
- VerticesGeo (functionality included in Vertices)
- PolygonGeo (functionality included in Polygon)
- SwmmInputGeo (functionality included in SwmmInput)

### Changed

- SwmmInput init is equal to SwmmInput.read_file
- better repr for SwmmInput and InpSections
- map plot function is separated in several function for better customization

### New

- swmm_api.input_file.macros.compare_inp_objects()
- add_backdrop to map plot
- Astlingen model in examples
- Examples for SNP10 conference
- get_used_curves
- check_outfall_connections
- detect_encoding for reading the inp-file and the rpt-file (default=utf-8)
- SwmmInput.read_text
- read out file as buffer
- input_file.macros.update_area
- error message when swmm is not found

### Fix

- inp update error
- minor issues with GIS import end export
- sort warnings in report by natural order
- minor issues with SwmmReport
- issue with networkx with parallel links
- possibility to have multiple times the same section in the inp-file
- swmm error when inline timeseries is on the end of the file
- error in controls
- error in timerseries, for specific orders

An error will be raised when calling a geo-function and the proper packages are not installed.

Renamed every parameter of the base Objects of the inp-sections.
The reason for the renaming is that the naming was previously very inconsistent and did not comply with the pip-standard.

## 0.2.0.18.3 (Mar 01, 2022)

Minor fixes
Fixed Error when using Timeseries past the year 3000.

## 0.2.0.18 (Feb 22, 2022)

### Fix

- type in swmm_api.input_file.macros.collection.subcachtment_nodes_dict > subcatchment_nodes_dict
- copy error for swmm_api.input_file.sections.lid.LIDControl, swmm_api.input_file.sections.others.Hydrograph, SnowPack
- reduce_controls now works
- error when reading a RDII
- infiltration object type recognition
- error when reading the report section "routing time step summary"

### Renamed
- in swmm_api.input_file.macros.collection subcatchment_nodes_dict to subcatchments_per_node_dict

### Added
- function swmm_api.input_file.macros.edit.remove_quality_model
- swmm_api.input_file.section_list.POLLUTANT_SECTIONS
- missing report sections
- swmm_api.input_file.macros.check.check_for_subcatchment_outlets
- swmm_api.input_file.macros.collection.nodes_subcatchments_dict
- InpSection and InpSectionGeneric have now _label for the section label
- add_new_section to SwmmInput
- SwmmInputGeo as alias of SwmmInput with geo_section_converter as custom_converter
- example for sorting in the inp-file-writer
- possibility to turn off sorting in inp-file write_file (`sort_objects_alphabetical=False`)
- SwmmHotstart file reader

### Moved
- SEC from swmm_api.input_file.section_abr to swmm_api.input_file

### Improved
- performance for reading bis inp-files
- natural sorting for objects. (i.e. the object names \[J1, J2, J10\] were previously sorted as  \[J1, J10, J2\])
- sections will be sorted as in the read file
- default sorting is based on sorting of the EPA SWMM GUI / PCSWMM
- copy unconverted inp file as string

### Changed
- in swmm_api.input_file.macros.check the functions check_for_nodes, check_for_duplicates now return set of error and don't print
- BaseSectionObject are now hashable
- Control object has now objects as action and condition for better usability


## 0.2.0.17 (Feb 11, 2022)
### Fixed
- error in copy Pollutant
- error in TimeseriesData when datetime is a float

### Renamed
- in swmm_api.input_file.macros.geo update_vertices to complete_vertices 

### Added
- swmm_api.input_file.macros.edit.flip_link_direction
- swmm_api.input_file.macros.geo.complete_link_vertices
- swmm_api.input_file.macros.geo.simplify_link_vertices
- swmm_api.input_file.macros.geo.simplify_vertices
- automatic creation for sections when getter is called and not in inp-data

### Moved
- reduce_vertices from swmm_api.input_file.macros.reduce_unneeded to geo

## 0.2.0.16 (Jan 7, 2022)
- moved predefined output file variables (VARIABLES, OBJECTS) to swmm_api.output_file.definitions
- new functions:
- swmm_api.input_file.macros.iter_sections
- swmm_api.input_file.macros.delete_sections
- added functions `add_obj` and `add_multiple` to SwmmInput object
- added function `delete_tag_group` to delete tags for specific objects i.e. all node tags
- `SEC` as reference for inp-sections
- remove ignore_sections, convert_sections and ignore_gui_sections parameters of swmm_api.SwmmInput.read_file
  - sections with be converted wenn needed.
- added function
  - SwmmInput.force_convert_all()
- added `SUBCATCHMENT_SECTIONS` to `swmm_api.input_file.section_lists`
- 

## 0.2.0.15 (Nov 19, 2021)
- added functions in swmm_api.input_file.macros.*
- added documentation for marcos
- minor changes

## 0.2.0.14 (Nov 11, 2021)
- New `TITLE` Section `TitleSection` based on `UserString`
- Default Infiltration based on `OPTIONS` - `INFILTRATION` parameter (function: `SwmmInput.set_default_infiltration`)
- added `InpSection.set_parent_inp` and `InpSection.get_parent_inp` to InpSections

## 0.2.0.13 (Oct 21, 2021)
- SwmmOutExtract.get_selective_results small performance boost
- new function: update_length
- updated and add example files
- resorted macros
- added macros with package `SWMM_xsections_shape_generator`
- added Summary tables to SwmmReport reader
- fixed some issued with SwmmReport
- new function: `check_for_duplicates`

## 0.2.0.12 (Sep 28, 2021)
- fixed out reader for custom pollutant unit
- gis import example
- LINK_SECTIONS, NODE_SECTIONS as section list
- SnowPack as new Class for reader
- datetime format fixed for import and export
- new "check_for_duplicates" macro
- fixed run for linux
- new get_swmm_version function
- pyswmm runner (not stable)
- fixed docker for documentation site

## 0.2.0.6 (Sep 15, 2021)
- better gis export
- compare inp files
- macro documentations

## 0.2.0.5 (Sep 10, 2021)
- gis export of all nodes, links as separate function
- added subcatchment connector to gis export
- added inp write and inp to string to SwmmInput class
- abstract class for nodes and links

## 0.2.0.4 (May 27, 2021)

- fixed errors when object labels start with a number
- rewritten out-file-reader

## 0.2.0.3 (May 18, 2021)

- fixed errors when `-nan(ind)` in report file

## 0.2.0.2 (May 5, 2021)

- added polygons to transform coordinated function
- added geopandas_to_polygons
- fixed some documentation errors
- fixed tag error (spaces in tag)
- fixed undefined types in some objects
- added function "add_inp_lines" to add a collection of lines to an existing section
- set default of "ignore_gui_sections" in "SwmmInput.read_file" to False
- added "delete_subcatchment" to macros
- fixed faulty tag filter in filter_nodes/_links/_subcatchments

## 0.2 (Apr 6, 2021)

## 0.1a25  (Apr 1, 2021)

## 0.1a24  (Mar 30, 2021)

## 0.1a23  (Feb 19, 2021)

## 0.1a22  (Dec 15, 2020)

## 0.1a21  (Nov 18, 2020)

Changes Including:
- 0.1a20  (Nov 9, 2020)
- 0.1a19  (Nov 6, 2020)
- 0.1a18  (Nov 6, 2020)

## 0.1a17  (Oct 16, 2020)

Changes Including:
- 0.1a16  (Sep 30, 2020)
- 0.1a15  (Sep 24, 2020)
- 0.1a14  (Sep 23, 2020)
- 0.1a13  (Sep 23, 2020)
- 0.1a12  (Sep 23, 2020)
- 0.1a11  (Sep 14, 2020)

## 0.1a10  (Aug 27, 2020)

Changes Including:
- 0.1a9  (Aug 27, 2020)

## 0.1a8  (Apr 19, 2020)

Changes Including:
- 0.1a7  (Apr 19, 2020)

## 0.1a6  (Nov 13, 2019)

Changes Including:
- 0.1a5  (Nov 8, 2019)
- 0.1a4  (Nov 4, 2019)
- 0.1a3  (Oct 28, 2019)
- 0.1a2  (Oct 3, 2019)
- 0.1a0  (Oct 2, 2019)
