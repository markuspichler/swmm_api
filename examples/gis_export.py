from examples.epaswmm5_apps_manual import PATH_EXAMPLES
from swmm_api.input_file.macros.gis import convert_inp_to_geo_package

if __name__ == '__main__':
    # inp = convert_inp_to_geo_package(inp_fn=PATH_EXAMPLES / 'Example1.inp',
    #                                  gpkg_fn=PATH_EXAMPLES / 'Example1.gpkg')

    inp = convert_inp_to_geo_package(inp_fn=PATH_EXAMPLES / 'Example6-Final_AllSections_GUI' / 'Example6-Final_AllSections_GUI.inp',
                                     gpkg_fn=PATH_EXAMPLES / 'Example6-Final_AllSections_GUI' / 'Example6-Final_AllSections_GUI.gpkg')
