from pathlib import Path

import pandas as pd
from tqdm.auto import tqdm

from examples.epa_swmm_samples_5_2 import PATH_EXAMPLES as PATH_EXAMPLES_5_2
from examples.epaswmm5_apps_manual import PATH_EXAMPLES as PATH_EXAMPLES_MANUAL
from swmm_api import read_inp_file
from swmm_api.input_file import section_labels

sections = {}

for folder in tqdm([PATH_EXAMPLES_MANUAL, PATH_EXAMPLES_5_2]):  # type: Path
    for fn in folder.iterdir():
        if '.inp' in fn.suffix:
            inp = read_inp_file(fn)
            print(fn)
            if 'projects' in str(folder):
                fn = 'projects/' + fn
            else:
                fn = 'examples/' + fn
            sections[fn] = {i: int(i in inp) for i in vars(section_labels).keys() if not i.startswith('_')}

df = pd.DataFrame(sections).T
df.to_excel('sections_in_examples.xlsx')
