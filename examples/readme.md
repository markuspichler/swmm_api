The folder `epa_swmm_samples_5_2` will be copied in the `~/Documents/EPA SWMM Projects/Samples` folder when SWMM version 5.2 is installed.

- `Culvert_Model.inp`
- `Culvert_Model.txt`
- `Detention_Pond_Model.inp`
- `Detention_Pond_Model.txt`
- `Groundwater_Model.inp`
- `Groundwater_Model.txt`
- `Inlet_Drains_Model.inp`
- `Inlet_Drains_Model.txt`
- `LID_Model.inp`
- `LID_Model.txt`
- `Pump_Control_Model.inp`
- `Pump_Control_Model.txt`
- `Site_Drainage_Model.inp`
- `Site_Drainage_Model.txt`

- `Site-Post.jpg`
- `sta310301.dat`

The folder `epa_swmm_examples_5_1` will be copied in the `~/Documents/EPA SWMM Projects/Examples` folder when SWMM version 5.1 is installed.

- `Example1.inp` (Site Drainage Model)
- `Example1.txt`
- `Example2.inp` (SWMM 3 Extran Comparison)
- `Example2.txt`
- `Example3.inp` (Pump Control Model)
- `Example3.txt`
- `Example4.inp` (Low Impact Development Model)
- `Example4.txt`
- `Example5.inp` (Groundwater Model)
- `Example5.txt`
- `Example6.inp` (Culvert_Model)
- `Example6.txt`

- `Extran1.dat`
- `sta310301.dat`

The files in the folder `epaswmm5_apps_manual` are from the EPA website for SWMM https://www.epa.gov/water-research/storm-water-management-model-swmm -> SWMM Applications Manual (zip) https://www.epa.gov/sites/production/files/2014-05/epaswmm5_apps_manual.zip

- `Example1-Post.inp`
- `Example1-Pre.inp`
- `Example2-Post.inp`
- `Example3.inp`
- `Example4.inp`
- `Example5-EMC.inp`
- `Example5-EXP.inp`
- `Example6-Final.inp`
- `Example6-Initial.inp`
- `Example7-Final.inp`
- `Example7-Initial.inp`
- `Example8.inp`
- `Example9.inp`
- `Record.dat`
- `Site-Post.jpg`
- `Site-Post-LID.jpg`
- `Site-Pre.jpg`

Another example model would be Bellinge https://doi.org/10.5194/essd-13-4779-2021 https://doi.org/10.11583/DTU.12513428

Or Astlingen https://github.com/open-toolbox/SWMM-Astlingen

Or GisToSWMM5 demo catchment https://doi.org/10.1061/(ASCE)HE.1943-5584.0001784