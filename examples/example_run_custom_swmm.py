import pandas as pd
from numpy import nan

from swmm_api.output_file.definitions import LINK_VARIABLES, OBJECTS, VARIABLES

from swmm_api.input_file.macros import conduit_slopes
from swmm_api.report_file.helpers import _part_to_frame
from swmm_api.run_swmm import swmm5_run_epa

from swmm_api import CONFIG, SwmmInput, SwmmReport


def main():
    # Custom SWMM binary from following repository
    # https://github.com/MarkusPic/Stormwater-Management-Model_hydraulic-Radius
    CONFIG.exe_path = '/Users/markus/CLionProjects/Stormwater-Management-Model/build/bin/runswmm'

    inp = SwmmInput('./epaswmm5_apps_manual/Example6-Final.inp')

    inp.OPTIONS['MINIMUM_STEP'] = 1
    inp.OPTIONS['ROUTING_STEP'] = '00:00:01'
    inp.OPTIONS['REPORT_STEP'] = '00:00:01'

    inp.REPORT.set_input(True)
    inp.REPORT.set_flowstats(True)

    # class LINK_VARIABLES_(LINK_VARIABLES):
    #     HYDRAULIC_RADIUS = 'hydraulic_radius'

    LINK_VARIABLES.extend('hydraulic_radius')

    # VARIABLES.LINK = LINK_VARIABLES_

    # LINK_VARIABLES.HYDRAULIC_RADIUS = 'hydraulic radius'  # in meters
    # LINK_VARIABLES.LIST_ += [LINK_VARIABLES.HYDRAULIC_RADIUS]

    def _conduit_shear_stress_summary(self: SwmmReport):
        if self._conduit_surcharge_summary is None:
            p = self._get_converted_part('Conduit Shear Stress Summary')
            self._conduit_surcharge_summary = _part_to_frame(p)
        return self._conduit_surcharge_summary

    SwmmReport.conduit_shear_stress_summary = property(_conduit_shear_stress_summary)

    from swmm_api.run_swmm.run_temporary import swmm5_run_temporary

    with swmm5_run_temporary(inp, run=swmm5_run_epa, label='example_run_swmm') as res:
        hydraulic_radius = res.out.get_part(OBJECTS.LINK, None, VARIABLES.LINK.HYDRAULIC_RADIUS).replace(-999., nan)  # in feet
        depth = res.out.get_part(OBJECTS.LINK, None, VARIABLES.LINK.DEPTH)  # in feet
        slopes = conduit_slopes(inp)
        shear_stress = 1000 * 9.81 * hydraulic_radius * 0.3048 * slopes  # in N/m2
        # ---
        shear_stress.max()
        shear_stress_max = res.rpt.conduit_shear_stress_summary

        df = pd.concat([
            shear_stress.max().rename('from out'),
            shear_stress_max.squeeze('columns').rename('from rpt')
        ], axis=1)
        print(df)


if __name__ == '__main__':
    main()
