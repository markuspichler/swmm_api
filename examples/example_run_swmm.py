from swmm_api.run_swmm import swmm5_run_progress, swmm5_run_epa

# run SWMM with pyswmm+tqdm progress bar
swmm5_run_progress('epaswmm5_apps_manual/Example6-Final.inp')

# ---

# run SWMM  using executable defined above
swmm5_run_epa('./epaswmm5_apps_manual/Example6-Final.inp', init_print=True)
