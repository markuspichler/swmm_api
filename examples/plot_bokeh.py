from examples.epaswmm5_apps_manual import PATH_EXAMPLES
from swmm_api import read_inp_file
from swmm_api.input_file.macros.plotting_map_bokeh import plot_map
from bokeh.plotting import save


inp = read_inp_file(PATH_EXAMPLES / 'Example6-Final_MP.inp')

fig = plot_map(inp)
save(fig, filename='test.html', title='Example6-Final_MP.inp')

# http://louistiao.me/posts/notebooks/embedding-matplotlib-animations-in-jupyter-notebooks/
