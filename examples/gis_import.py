from swmm_api.input_file.macros.gis import gpkg_to_swmm
from swmm_api.input_file.sections import InfiltrationGreenAmpt

if __name__ == '__main__':
    # inp = gpkg_to_swmm('/mnt/Windows/Users/mp/GIS/inp_update.gpkg')
    # inp = gpkg_to_swmm('Example1.gpkg')
    inp = gpkg_to_swmm('/Users/markus/Downloads/gdf_model_1.gpkg', label_sep='.', infiltration_class=InfiltrationGreenAmpt, custom_section_handler=None, simplify=False)
