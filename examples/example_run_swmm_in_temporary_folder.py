from swmm_api.run_swmm import swmm5_run_epa
from swmm_api import SwmmInput, SwmmOutput, SwmmReport
from swmm_api.run_swmm.run_temporary import swmm5_run_temporary

inp = SwmmInput('epaswmm5_apps_manual/Example6-Final_AllSections_GUI/Example6-Final_AllSections_GUI.inp')

with swmm5_run_temporary(inp, run=swmm5_run_epa, label='example_run_swmm') as res:
    res.out  # type: SwmmOutput
    res.rpt  # type: SwmmReport
    res.inp  # type: SwmmInput
    res._parent_path  # type: pathlib.Path # temporary folder - will be deleted after "with" range
    di_lid = res.lid_rpt_dict
