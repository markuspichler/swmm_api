from swmm_api import read_out_file, swmm5_run, SwmmOutput
from swmm_api.output_file import OBJECTS, VARIABLES


# Example for OUT File Reader

# swmm5_run('epaswmm5_apps_manual/Example6-Final.inp')

# out = SwmmOutput('test_long_broken.out')
out = read_out_file('epaswmm5_apps_manual/Example6-Final.out')

# testing start and end times for sliced reading
start_i = 123
start = out.index[start_i]
# start = d.index[5]
end_i = 982
end = out.index[end_i]
# end = d.index[10]

out.index[start_i:end_i+1]

out._frame = None
out._data = None
d = out.get_part(OBJECTS.NODE, None, VARIABLES.NODE.HEAD, slim=True, start=start)
assert d.index[0] == start
assert d.index[-1] == out.index[-1]

out._frame = None
out._data = None
d = out.get_part(OBJECTS.NODE, None, VARIABLES.NODE.HEAD, slim=False, start=start)
assert d.index[0] == start
assert d.index[-1] == out.index[-1]

out._frame = None
out._data = None
d = out.get_part(OBJECTS.NODE, None, VARIABLES.NODE.HEAD, slim=True, end=end)
assert d.index[0] == out.index[0]
assert d.index[-1] == end

d = out.get_part(OBJECTS.NODE, None, VARIABLES.NODE.HEAD, slim=False, end=end)

# reading data for a specific time range
d2_ = out.get_part(OBJECTS.NODE, None, VARIABLES.NODE.HEAD, slim=False, start=start, end=end)
d_ = out.get_part(OBJECTS.NODE, None, VARIABLES.NODE.HEAD, slim=True, start=start, end=end)

# read all node heads using the python engine (slower but able to read huge files)
d = out.get_part(OBJECTS.NODE, None, VARIABLES.NODE.HEAD, slim=True)

# read all node heads using the numpy (C) engine (faster but uses a lot of memory for huge file and thus may fail)
d2 = out.get_part(OBJECTS.NODE, None, VARIABLES.NODE.HEAD, slim=False)

d2_ = out.get_part(OBJECTS.NODE, None, VARIABLES.NODE.HEAD, slim=False, start=start)

d2_ = out.get_part(OBJECTS.NODE, None, VARIABLES.NODE.HEAD, slim=False, end=end)

# reading data for a specific time range
d2_ = out.get_part(OBJECTS.NODE, None, VARIABLES.NODE.HEAD, slim=False, start=start, end=end)




# n_cols = 1612
# c: 10s
# py: 28s
# ---
# n_cols = 9672
# c: 51s
# py: 2m 40s

out.get_part(OBJECTS.NODE, 'J1', VARIABLES.NODE.HEAD, slim=True)

out.get_part(OBJECTS.NODE, 'J1', VARIABLES.NODE.HEAD).to_frame()
out.get_part(OBJECTS.NODE, ['J1', 'J23fsd'], VARIABLES.NODE.HEAD).to_frame()
out.get_part(OBJECTS.NODE, ['J1', 'J23fsd'], [VARIABLES.NODE.HEAD, 'fj1e']).to_frame()

# get all data as pandas.DataFrame
out.to_frame()

# get a specific part of the out data as pandas.Series
out.get_part(OBJECTS.NODE, 'J1', VARIABLES.NODE.HEAD).to_frame()

# to get all data of a node, just remove the variable part
out.get_part(OBJECTS.NODE, 'J1')

# other functions
out.filename

out.variables

out.labels

out.number_columns

type(out.to_numpy())
