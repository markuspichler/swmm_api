from pathlib import Path
from tqdm.auto import tqdm

import warnings

from examples.epa_swmm_examples_5_1 import PATH_EXAMPLES as PATH_EXAMPLES_5_1
from examples.epa_swmm_samples_5_2 import PATH_EXAMPLES as PATH_EXAMPLES_5_2
from examples.epaswmm5_apps_manual import PATH_EXAMPLES as PATH_EXAMPLES_MANUAL
from swmm_api import SwmmInput, SwmmReport, SwmmOutput, CONFIG
from swmm_api.input_file import SEC
from swmm_api.input_file.helpers import SwmmInputWarning
from swmm_api.run_swmm import SWMMRunError, swmm5_run_temporary, swmm5_run_owa
from swmm_api.run_swmm.run_epaswmm import get_swmm_version_epa


# CONFIG.exe_path = r"C:\Program Files\EPA SWMM 5.2.3 (64-bit)\runswmm.exe"
# CONFIGencoding = 'ISO-8859-1'
CONFIG.exe_path = "/Users/markus/.bin/runswmm"

warnings.filterwarnings('ignore', category=SwmmInputWarning)

"""if no error occur pretty much everything works (or more test cases are needed)"""

parent_dir = Path(__file__).parent
example_dirs = [PATH_EXAMPLES_MANUAL,
                PATH_EXAMPLES_5_1,
                PATH_EXAMPLES_5_2,
                PATH_EXAMPLES_MANUAL / 'Example6-Final_AllSections_GUI']

version = get_swmm_version_epa()
print(f'Version: {version}')
print()

example_files = [fn for folder in example_dirs for fn in folder.iterdir() if fn.suffix == '.inp']


def _convert_all(rpt):
    keys = ['analyse_duration',
            'analyse_end',
            'analyse_start',
            'analysis_options',
            'conduit_surcharge_summary',
            'cross_section_summary',
            'flow_classification_summary',
            'flow_routing_continuity',
            'flow_unit',
            'get_errors',
            'get_simulation_info',
            'get_warnings',
            'highest_continuity_errors',
            'highest_flow_instability_indexes',
            'link_flow_summary',
            'link_summary',
            'node_depth_summary',
            'node_flooding_summary',
            'node_inflow_summary',
            'node_summary',
            'node_surcharge_summary',
            'outfall_loading_summary',
            'rainfall_file_summary',
            'raingage_summary',
            'runoff_quantity_continuity',
            'storage_volume_summary',
            'subcatchment_runoff_summary',
            'subcatchment_summary',
            'time_step_critical_elements',
            'control_actions_taken',
            'element_count',
            'groundwater_continuity',
            'groundwater_summary',
            'landuse_summary',
            'lid_control_summary',
            'lid_performance_summary',
            'link_pollutant_load_summary',
            'note',
            'pollutant_summary',
            'pumping_summary',
            'quality_routing_continuity',
            'routing_time_step_summary',
            'runoff_quality_continuity',
            'subcatchment_washoff_summary',
            'transect_summary'
            ]

    for key in keys:
        rpt.__getattribute__(key)


failed = []

with tqdm(example_files, desc='TESTING_ALL_EXAMPLES') as example_files:
    for fn in example_files:  # type: Path
        example_files.set_postfix_str(str(list(example_files.iterable)[example_files.n]))

        if version != '5.1.15' and fn.name.endswith('Example1_smm5-1-15.inp'):
            continue

        if '5.2' not in version and 'Samples' in fn:
            continue

        # READ
        inp = SwmmInput(fn)

        # MANIPULATE

        # convert all
        inp.force_convert_all()

        # test copy
        inp.copy()

        if SEC.RAINGAGES in inp:
            if 'RainGage' in inp.RAINGAGES:
                # if isinstance(inp.RAINGAGES['RainGage'].Filename, str):
                #     print()
                if inp.RAINGAGES['RainGage'].filename == 'Record.dat':
                    inp.RAINGAGES['RainGage'].filename = str(parent_dir / 'epaswmm5_apps_manual' / inp.RAINGAGES['RainGage'].filename)
                    pass # C:\Users\mp\PycharmProjects\swmm_api\examples\epaswmm5_apps_manual\

        # WRITE
        inp.REPORT['INPUT'] = True
        inp.REPORT['CONTINUITY'] = True
        inp.REPORT['FLOWSTATS'] = True
        inp.REPORT['CONTROLS'] = True
        inp.REPORT['SUBCATCHMENTS'] = 'ALL'
        inp.REPORT['NODES'] = 'ALL'
        inp.REPORT['LINKS'] = 'ALL'

        # RUN
        try:
            with swmm5_run_temporary(inp.copy(), run=swmm5_run_owa, label='example_run_swmm') as res:
                out = res.out  # type: SwmmOutput
                rpt = res.rpt  # type: SwmmReport
        except SWMMRunError as e:
            failed.append(f'{fn}{e}')
            continue
        except UnicodeDecodeError as e:
            failed.append(f'{fn} ({e})')
            continue

        # REPORT
        _convert_all(rpt)
        del rpt

        # OUTPUT
        out.to_numpy()

print('FAILED:', *failed, sep='\n  - ')
