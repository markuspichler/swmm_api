#%% md

## Example for INP File Macros

#%%

from swmm_api import read_inp_file
from swmm_api.input_file.macros import plot_map, plot_longitudinal, write_geo_package, init_empty_map_plot, add_link_map, add_subcatchment_map, add_backdrop, add_node_map, add_node_labels, nodes_dict

#%%

inp = read_inp_file('epaswmm5_apps_manual/Example6-Final.inp')


# write_geo_package(inp, 'test.gpkg')
# exit()
#%%

fig, ax = init_empty_map_plot()

# add_link_map(ax, inp, add_arrows=True, values_dict={k: v.height for k, v in inp.XSECTIONS.items()},
#              make_width_proportional=False, line_width_max=5)

# add_link_map(ax, inp, add_arrows=True, values_dict={k: v.height for k, v in inp.XSECTIONS.items()},
#              make_width_proportional=True, line_width_max=5, cmap='cividis', colorbar_kwargs={'label': 'height in ft'})

add_link_map(ax, inp, add_arrows=True, values_dict={k: v.height for k, v in inp.XSECTIONS.items()},
             make_width_proportional=False, line_width_max=5, cmap='cividis',
             colorbar_kwargs={'label': 'height in ft', 'loc': 'lower left'}, discrete=True)

# add_link_map(ax, inp, add_arrows=True)
# add_link_map(ax, inp, add_arrows=True, values_dict={k: v.shape for k, v in inp.XSECTIONS.items()},
#              cmap='rainbow', colorbar_kwargs={'label': 'Shape'}, discrete=True)

# ---
# add_subcatchment_map(ax, inp, add_connector_arrows=True, linewidth=0.5, alpha=0.5, fill=True,
#                      values_dict={k: v.slope for k, v in inp.SUBCATCHMENTS.items()}, discrete=False, colorbar_kwargs={'label': 'slope (%)'}, cmap='rainbow')

add_subcatchment_map(ax, inp, add_connector_line=False, add_center_point=False)


# add_subcatchment_map(ax, inp, add_connector_arrows=True, linewidth=0.5, alpha=0.5, fill=True,
#                      values_dict={k: v.rain_gage for k, v in inp.SUBCATCHMENTS.items()},
#                      discrete=True, colorbar_kwargs={'title': 'Rain Gage'}, cmap='rainbow')

add_node_map(ax, inp)
# add_node_map(ax, inp, values_dict={k: v.elevation for k, v in nodes_dict(inp).items()}, cmap='rainbow', colorbar_kwargs={'label': 'elevation'})
# add_node_map(ax, inp, values_dict={k: v.elevation for k, v in nodes_dict(inp).items()},
#              cmap='rainbow', colorbar_kwargs={'label': 'elevation'}, discrete=False)
# add_node_map(ax, inp, values_dict={k: v.kind if hasattr(v, 'kind') else 'Junction' for k, v in nodes_dict(inp).items()},
#              cmap='rainbow', colorbar_kwargs={'title': 'kind'}, discrete=True)

add_node_labels(ax, inp, size=5, y_offset=15)

fig.savefig('epaswmm5_apps_manual/Example6-Final.png', dpi=300)
fig.savefig('epaswmm5_apps_manual/Example6-Final.pdf')

#%%

fig, ax = plot_longitudinal(inp, start_node='J12', end_node='O2', out=None, ax=None, zero_node=None)
# ax.set_title("start_node='9', end_node='18'")

