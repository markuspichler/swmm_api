import pandas as pd
import numpy as np
from swmm_api import SwmmInput
from swmm_api.input_file.macros import reduce_controls, delete_node, rename_node, remove_empty_sections


def main():
    # %%
    inp = SwmmInput('control_rules.inp')
    # %%
    print(inp.CONTROLS.to_inp_lines())
    print('#'*50)
    # %%
    reduce_controls(inp, verbose=True)
    print(inp.CONTROLS.to_inp_lines())
    print('#'*50)
    # %%
    inp_ = inp.copy()
    delete_node(inp_, '1')
    reduce_controls(inp_, verbose=False)
    print(inp_.CONTROLS.to_inp_lines())
    print('#' * 50)
    # %%
    inp_ = inp.copy()
    rename_node(inp_, '1', 'new_node_1')
    reduce_controls(inp_, verbose=False)
    print(inp_.CONTROLS.to_inp_lines())


if __name__ == '__main__':
    main()
