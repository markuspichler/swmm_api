=================
Input File Reader
=================
.. currentmodule:: swmm_api

Constructor
~~~~~~~~~~~
.. autosummary::
    :toctree: inp/

    SwmmInput
    read_inp_file