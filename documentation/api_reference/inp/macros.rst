Input File Manipulation - Macros
--------------------------------

Check
~~~~~

.. currentmodule:: swmm_api.input_file.macros.check
.. autosummary::
    :toctree: macros/

    check_for_curves
    check_for_duplicate_links
    check_for_duplicate_nodes
    check_for_duplicates
    check_for_nodes
    check_for_nodes_old
    check_for_subcatchment_outlets
    check_for_subcatchment_outlets_old
    check_outfall_connections

Collection
~~~~~~~~~~

.. currentmodule:: swmm_api.input_file.macros.collection
.. autosummary::
    :toctree: macros/

    links_dict
    nodes_dict
    nodes_subcatchments_dict
    subcatchments_per_node_dict

Combine Dwf
~~~~~~~~~~~

.. currentmodule:: swmm_api.input_file.macros.combine_dwf
.. autosummary::
    :toctree: macros/

    combine_dwf
    get_dwf_node_series
    get_pattern_series_prod
    get_pattern_time_frame
    get_weekend_bool_series
    prep_factors

Compare
~~~~~~~

.. currentmodule:: swmm_api.input_file.macros.compare
.. autosummary::
    :toctree: macros/

    compare_inp_files
    compare_inp_objects
    compare_sections
    inp_version_control
    reverse_dict

Convert Model
~~~~~~~~~~~~~

.. currentmodule:: swmm_api.input_file.macros.convert_model
.. autosummary::
    :toctree: macros/

    to_kinematic_wave

Convert Object
~~~~~~~~~~~~~~

.. currentmodule:: swmm_api.input_file.macros.convert_object
.. autosummary::
    :toctree: macros/

    conduit_to_orifice
    junction_to_divider
    junction_to_outfall
    junction_to_storage
    orifice_to_conduit
    storage_to_outfall

Cross Section Curve
~~~~~~~~~~~~~~~~~~~

.. currentmodule:: swmm_api.input_file.macros.cross_section_curve
.. autosummary::
    :toctree: macros/

    get_cross_section_maker
    profil_area
    to_cross_section_maker

Curve
~~~~~

.. currentmodule:: swmm_api.input_file.macros.curve
.. autosummary::
    :toctree: macros/

    curve_figure

Cut Model
~~~~~~~~~

.. currentmodule:: swmm_api.input_file.macros.cut_model
.. autosummary::
    :toctree: macros/

    create_cut_model

Edit
~~~~

.. currentmodule:: swmm_api.input_file.macros.edit
.. autosummary::
    :toctree: macros/

    combine_conduits
    combine_conduits_keep_slope
    combine_vertices
    copy_link
    copy_node
    delete_link
    delete_node
    delete_pollutant
    delete_subcatchment
    dissolve_conduit
    flip_link_direction
    move_flows
    reconnect_subcatchments
    remove_obj_from_control
    remove_obj_from_reporting
    remove_obj_tag
    remove_quality_model
    rename_link
    rename_node
    rename_obj_in_control_section
    rename_obj_in_reporting_section
    rename_obj_in_tags
    rename_subcatchment
    rename_timeseries
    split_conduit

Filter
~~~~~~

.. currentmodule:: swmm_api.input_file.macros.filter
.. autosummary::
    :toctree: macros/

    create_sub_inp
    filter_links
    filter_links_within_nodes
    filter_nodes
    filter_subcatchments
    filter_tags

Geo
~~~

.. currentmodule:: swmm_api.input_file.macros.geo
.. autosummary::
    :toctree: macros/

    complete_link_vertices
    complete_vertices
    reduce_vertices
    simplify_link_vertices
    simplify_vertices
    transform_coordinates

Gis
~~~

.. currentmodule:: swmm_api.input_file.macros.gis
.. autosummary::
    :toctree: macros/

    apply_gis_style_to_gpkg
    convert_inp_to_geo_package
    get_subcatchment_connectors
    gpkg_to_swmm
    links_geo_data_frame
    nodes_geo_data_frame
    set_crs
    subcatchment_geo_data_frame
    update_area
    update_length
    write_geo_package

Graph
~~~~~

.. currentmodule:: swmm_api.input_file.macros.graph
.. autosummary::
    :toctree: macros/

    conduit_iter_over_inp
    get_downstream_nodes
    get_downstream_path
    get_network_forks
    get_path
    get_path_subgraph
    get_upstream_nodes
    inp_to_graph
    links_connected
    next_links
    next_links_labels
    next_nodes
    number_in_out
    previous_links
    previous_links_labels
    previous_nodes
    split_network
    subcatchments_connected

Macros
~~~~~~

.. currentmodule:: swmm_api.input_file.macros.macros
.. autosummary::
    :toctree: macros/

    calc_slope
    combined_nodes_frame
    combined_subcatchment_frame
    conduit_slopes
    conduits_are_equal
    convert_cms_to_lps
    delete_sections
    find_link
    find_node
    increase_max_node_depth
    iter_sections
    nodes_data_frame
    set_absolute_file_paths
    set_times
    update_no_duplicates

Plotting Longitudinal
~~~~~~~~~~~~~~~~~~~~~

.. currentmodule:: swmm_api.input_file.macros.plotting_longitudinal
.. autosummary::
    :toctree: macros/

    animated_plot_longitudinal
    get_longitudinal_data
    get_node_station
    get_water_level
    iter_over_inp
    iter_over_inp_
    jupyter_animated_plot_longitudinal
    plot_longitudinal
    set_zero_node

Plotting Map
~~~~~~~~~~~~

.. currentmodule:: swmm_api.input_file.macros.plotting_map
.. autosummary::
    :toctree: macros/

    add_backdrop
    add_custom_legend
    add_link_map
    add_node_labels
    add_node_map
    add_subcatchment_map
    custom_color_mapper
    get_auto_size_function
    get_color_mapper
    get_discrete_colormap
    get_matplotlib_colormap
    init_empty_map_plot
    plot_map
    set_inp_dimensions

Plotting Map Leaflet
~~~~~~~~~~~~~~~~~~~~

.. currentmodule:: swmm_api.input_file.macros.plotting_map_leaflet
.. autosummary::
    :toctree: macros/

    plot_map

Plotting Sub Map
~~~~~~~~~~~~~~~~

.. currentmodule:: swmm_api.input_file.macros.plotting_sub_map
.. autosummary::
    :toctree: macros/

    plot_sub_map

Reduce Unneeded
~~~~~~~~~~~~~~~

.. currentmodule:: swmm_api.input_file.macros.reduce_unneeded
.. autosummary::
    :toctree: macros/

    reduce_controls
    reduce_curves
    reduce_hydrographs
    reduce_pattern
    reduce_raingages
    reduce_report_objects
    reduce_snowpacks
    reduce_timeseries
    remove_empty_sections
    simplify_curves

Split Inp File
~~~~~~~~~~~~~~

.. currentmodule:: swmm_api.input_file.macros.split_inp_file
.. autosummary::
    :toctree: macros/

    read_split_inp_file
    split_inp_to_files

Summarize
~~~~~~~~~

.. currentmodule:: swmm_api.input_file.macros.summarize
.. autosummary::
    :toctree: macros/

    print_summary
    short_status

Tags
~~~~

.. currentmodule:: swmm_api.input_file.macros.tags
.. autosummary::
    :toctree: macros/

    delete_tag_group
    filter_tags
    get_link_tags
    get_node_tags
    get_subcatchment_tags
