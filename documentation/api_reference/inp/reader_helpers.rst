
Input File Helpers
------------------

.. currentmodule:: swmm_api.input_file.helpers

.. autosummary::
    :toctree: helpers/
    :recursive:

    BaseSectionObject
    InpSectionGeneric
    InpSection
    InpSectionGeo
