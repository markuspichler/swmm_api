import ast
from pathlib import Path

working_dir = Path(__file__).parent

import sys
sys.path.append(str(working_dir.parent.parent.parent))

from swmm_api.input_file import macros


def main():
    # module = dir(macros)
    # print(dir(macros))

    with open(working_dir / 'macros.rst', 'w') as f:

        _header = 'Input File Manipulation - Macros'
        f.write(_header + '\n')
        f.write('-'*len(_header) + '\n')

        for file in sorted(Path(macros.__file__).parent.iterdir()):
            if file.stem.startswith('_') or file.is_dir() or file.stem.startswith('.'):
                continue

            if 'plotting_map_plotly' in file.stem:
                continue
            if 'plotting_map_bokeh' in file.stem:
                continue

            # module_label = file.stem.replace('_', ' ').capitalize()
            module_label = ' '.join([f.capitalize() for f in file.stem.split('_')])

            f.write('\n' + module_label + '\n')
            f.write('~'*len(module_label) + '\n\n')

            print(module_label)
            f.write(f'.. currentmodule:: {macros.__package__}.{file.stem}\n')

            f.write('.. autosummary::\n')
            f.write('    :toctree: macros/\n\n')

            with open(file, encoding='utf-8') as _f:
                parts = [part.name for part in ast.parse(_f.read()).body if isinstance(part, ast.FunctionDef) and not part.name.startswith('_')]
                for part in sorted(parts):
                    f.write(f'    {part}\n')
                    # print(part)
                # for part in ast.parse(_f.read()).body:
                #     if isinstance(part, ast.FunctionDef):
                #         if not part.name.startswith('_'):
                #             print(part.name)
                #             f.write(f'    {part.name}\n')

if __name__ == '__main__':
    main()
