Hotstart File Reader
====================
.. currentmodule:: swmm_api

Constructor
~~~~~~~~~~~
.. autosummary::
    :toctree: hst/

    SwmmHotstart
    read_hst_file
