Running SWMM
============

Recommended function
--------------------

.. automodule:: swmm_api.run_swmm.run
    :members:
    :no-undoc-members:

-----

Using EPA-SWMM
--------------

.. automodule:: swmm_api.run_swmm.run_epaswmm
    :members:
    :no-undoc-members:

-----

Using OWA-SWMM
--------------

.. automodule:: swmm_api.run_swmm.run_swmm_toolkit
    :members:
    :no-undoc-members:

-----

Using PYSWMM
------------

.. automodule:: swmm_api.run_swmm.run_pyswmm
    :members:
    :no-undoc-members:
