#!/usr/bin/env bash

cp ../examples/inp_file_reader.ipynb ./examples/inp_file_reader.ipynb
cp ../examples/inp_file_macros.ipynb ./examples/inp_file_macros.ipynb
cp ../examples/inp_file_structure.ipynb ./examples/inp_file_structure.ipynb
cp ../examples/inp_file_writer.ipynb ./examples/inp_file_writer.ipynb
cp ../examples/rpt_file_reader.ipynb ./examples/rpt_file_reader.ipynb
cp ../examples/out_file_reader.ipynb ./examples/out_file_reader.ipynb
cp ../examples/hotstart_file_reader.ipynb ./examples/hotstart_file_reader.ipynb
cp ../examples/gis_data_to_swmm.ipynb ./examples/gis_data_to_swmm.ipynb
cp ../examples/how_to_add_control_rules.ipynb ./examples/how_to_add_control_rules.ipynb
cp ../examples/custom_inp_sections.ipynb ./examples/custom_inp_sections.ipynb
cp ../examples/example_timeseries_classes.ipynb ./examples/example_timeseries_classes.ipynb
cp ../examples/how_to_add_LIDs.ipynb ./examples/how_to_add_LIDs.ipynb
cp ../examples/special_sections.ipynb ./examples/special_sections.ipynb
cp ../examples/cross_section_plot.ipynb ./examples/cross_section_plot.ipynb
# ---
cp ../examples/epa_swmm_samples_5_2/Culvert_Model.ipynb  ./examples/Culvert_Model.ipynb
cp ../examples/epa_swmm_samples_5_2/Detention_Pond_Model.ipynb ./examples/Detention_Pond_Model.ipynb
cp ../examples/epa_swmm_samples_5_2/Groundwater_Model.ipynb ./examples/Groundwater_Model.ipynb
cp ../examples/epa_swmm_samples_5_2/Inlet_Drains_Model.ipynb ./examples/Inlet_Drains_Model.ipynb
cp ../examples/epa_swmm_samples_5_2/LID_Model.ipynb ./examples/LID_Model.ipynb
cp ../examples/epa_swmm_samples_5_2/Pump_Control_Model.ipynb ./examples/Pump_Control_Model.ipynb
cp ../examples/epa_swmm_samples_5_2/Site_Drainage_Model.ipynb ./examples/Site_Drainage_Model.ipynb
# ---
cp ../README.md ./README.md
cp ../CHANGES.md ./CHANGES.md
cp ../CONTRIBUTING.md ./CONTRIBUTING.md
# ---
#ln -s ../README.md ./README.md
#sed -i "s/This is an API for reading, manipulating and running SWMM-Projects/Getting started/" ./README.md

make html
