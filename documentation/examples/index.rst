========
Examples
========

.. toctree::
   :maxdepth: 2
   :caption: Content:

   inp_file_reader
   inp_file_macros
   inp_file_structure
   rpt_file_reader
   out_file_reader
   hotstart_file_reader
   gis_data_to_swmm
   custom_inp_sections
   example_timeseries_classes
   how_to_add_control_rules
   how_to_add_LIDs
   special_sections
   cross_section_plot

=================
EPA-SWMM Examples
=================

.. toctree::
   :maxdepth: 1
   :caption: Content:

   Culvert_Model
   Detention_Pond_Model
   Groundwater_Model
   Inlet_Drains_Model
   LID_Model
   Pump_Control_Model
   Site_Drainage_Model
